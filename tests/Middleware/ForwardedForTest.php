<?php

namespace Miniframe\Middleware;

use Miniframe\Core\Config;
use Miniframe\Core\Request;
use PHPUnit\Framework\TestCase;

class ForwardedForTest extends TestCase
{
    /**
     * Stores the original $_SERVER global so we can make sure all values are restored in it's initial state.
     *
     * @var string[]
     */
    private $originalRequest;

    /**
     * Reset the PHP Server API name before each test to prevent data from a previous test to be active
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $this->originalRequest = $_SERVER;
    }

    /**
     * This method is called after each test.
     *
     * @return void
     */
    protected function tearDown(): void
    {
        parent::tearDown();
        $_SERVER = $this->originalRequest;
    }

    /**
     * Dataprovider for testSet()
     *
     * @return array<
     *     string,
     *     array{
     *         'source_request': array<string, string>,
     *         'result_request': array<string, string|null>,
     *         'proxy_hosts': string[]|null
     *     }
     * >
     */
    public function dataProvider(): array
    {
        /**
         * Tests based on the examples at https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/X-Forwarded-For
         */
        $xForwardedForTests = array(
            'X-Forwarded-For with a single IPv6 address' => [
                'source_request' => [
                    'REMOTE_ADDR'          => '192.168.0.1',
                    'HTTP_X_FORWARDED_FOR' => '2001:db8:85a3:8d3:1319:8a2e:370:7348',
                ],
                'result_request' => [
                    'REMOTE_ADDR'          => '2001:db8:85a3:8d3:1319:8a2e:370:7348',
                    'HTTP_X_FORWARDED_FOR' => null,
                ],
                'proxy_hosts' => null,
            ],
            'X-Forwarded-For with a single IPv4 address' => [
                'source_request' => [
                    'REMOTE_ADDR'          => '192.168.0.1',
                    'HTTP_X_FORWARDED_FOR' => '203.0.113.195',
                ],
                'result_request' => [
                    'REMOTE_ADDR'          => '203.0.113.195',
                    'HTTP_X_FORWARDED_FOR' => null,
                ],
                'proxy_hosts' => null,
            ],
            'X-Forwarded-For with an IPv4 client address and an IPv6 proxy address' => [
                'source_request' => [
                    'REMOTE_ADDR'          => '192.168.0.1',
                    'HTTP_X_FORWARDED_FOR' => '203.0.113.195, 2001:db8:85a3:8d3:1319:8a2e:370:7348',
                ],
                'result_request' => [
                    'REMOTE_ADDR'          => '203.0.113.195',
                    'HTTP_X_FORWARDED_FOR' => null,
                ],
                'proxy_hosts' => null,
            ],
            'X-Forwarded-For with an IPv4 client address and two proxy addresses' => [
                'source_request' => [
                    'REMOTE_ADDR'          => '192.168.0.1',
                    'HTTP_X_FORWARDED_FOR' => '203.0.113.195,2001:db8:85a3:8d3:1319:8a2e:370:7348,198.51.100.178',
                ],
                'result_request' => [
                    'REMOTE_ADDR'          => '203.0.113.195',
                    'HTTP_X_FORWARDED_FOR' => null,
                ],
                'proxy_hosts' => null,
            ],
        );

        /**
         * Test based on https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/X-Forwarded-Host
         */
        $xForwardedHostTests = array(
            'X-Forwarded-Host' => [
                'source_request' => [
                    'REMOTE_ADDR'           => '192.168.0.1',
                    'HTTP_HOST'             => '127.0.0.1',
                    'HTTP_X_FORWARDED_HOST' => 'id42.example-cdn.com',
                ],
                'result_request' => [
                    'HTTP_HOST'             => 'id42.example-cdn.com',
                    'HTTP_X_FORWARDED_HOST' => null,
                ],
                'proxy_hosts' => null,
            ],
        );

        /**
         * See: https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/X-Forwarded-Proto
         */
        $xForwardedProtoTests = array(
            'X-Forwarded-Proto from http to https' => [
                'source_request' => [
                    'REMOTE_ADDR'            => '192.168.0.1',
                    'HTTP_X_FORWARDED_PROTO' => 'https',
                    'SERVER_PORT'            => '80',
                ],
                'result_request' => [
                    'HTTPS'                  => 'on',
                    'SERVER_PORT'            => '443',
                    'HTTP_X_FORWARDED_PROTO' => null,
                ],
                'proxy_hosts' => null,
            ],
            'X-Forwarded-Proto from https to http' => [
                'source_request' => [
                    'REMOTE_ADDR'            => '192.168.0.1',
                    'HTTP_X_FORWARDED_PROTO' => 'http',
                    'SERVER_PORT'            => '443',
                    'HTTPS'                  => 'on',
                ],
                'result_request' => [
                    'HTTPS'                  => null,
                    'SERVER_PORT'            => '80',
                    'HTTP_X_FORWARDED_PROTO' => null,
                ],
                'proxy_hosts' => null,
            ],
        );

        /**
         * Generic tst sets
         */
        $genericTests = array(
            'No X-Forwarded headers defined' => [
                'source_request' => [
                    'HTTP_HOST' => 'localhost',
                    'SERVER_PORT' => '80',
                    'REMOTE_ADDR' => '192.168.2.1',
                ],
                'result_request' => [
                    'HTTP_HOST' => 'localhost',
                    'SERVER_PORT' => '80',
                    'REMOTE_ADDR' => '192.168.2.1',
                ],
                'proxy_hosts' => null,
            ],
            'No remote address header defined (happens on console requests)' => [
                'source_request' => [
                    'HTTP_HOST' => 'localhost',
                    'SERVER_PORT' => '80',
                ],
                'result_request' => [
                    'HTTP_HOST' => 'localhost',
                    'SERVER_PORT' => '80',
                ],
                'proxy_hosts' => null,
            ],
        );

        /**
         * Valid/invalid proxy host tests
         */
        $validProxyHostTests = array();
        $invalidProxyHostTests = array();
        $proxyHosts = [
            '10.*.*.*',
            '192.168.*.*',
            '172.16.0.0/12',
        ];
        foreach (array_merge($xForwardedForTests, $xForwardedHostTests, $xForwardedProtoTests) as $key => $test) {
            $test['proxy_hosts'] = $proxyHosts;
            foreach (
                [
                '192.168.2.1',
                '10.1.2.4',
                '172.16.1.1',
                ] as $remoteAddress
            ) {
                $test['source_request']['REMOTE_ADDR'] = $remoteAddress;
                $validProxyHostTests['Proxy ' . $remoteAddress . ' - ' . $key] = $test;
            }
            foreach (
                [
                '8.8.8.8',
                '172.15.1.1',
                ] as $remoteAddress
            ) {
                $test['source_request']['REMOTE_ADDR'] = $remoteAddress;
                $test['result_request'] = $test['source_request'];
                $invalidProxyHostTests['Proxy ' . $remoteAddress . ' - ' . $key] = $test;
            }
        }

        return array_merge(
            $xForwardedForTests,
            $xForwardedProtoTests,
            $xForwardedHostTests,
            $genericTests,
            $validProxyHostTests,
            $invalidProxyHostTests,
        );
    }

    /**
     * Tests a single entry
     *
     * @param array<string, string>      $sourceRequest The original $_SERVER values.
     * @param array<string, string|null> $resultRequest The calculated $_SERVER values.
     * @param string[]|null              $proxyHosts    The proxy hosts config entry.
     *
     * @return void
     *
     * @dataProvider dataProvider
     */
    public function testSet(array $sourceRequest, array $resultRequest, array $proxyHosts = null): void
    {
        $sourceRequest = array_merge($sourceRequest, ['REQUEST_URI' => '/']);

        // Set request and super global
        $request = new Request($sourceRequest);
        $_SERVER = $sourceRequest;

        // Initialize middleware
        $forwardedFor = new ForwardedFor($request, $this->getDummyConfig($proxyHosts));

        // Assert resultset
        foreach ($resultRequest as $key => $value) {
            if ($value === null) {
                $this->assertArrayNotHasKey($key, $_SERVER, 'Server global ' . $key);
            } else {
                $this->assertEquals($value, $_SERVER[$key], 'Server global ' . $key);
            }
            $this->assertEquals($value, $request->getServer($key), 'Request value ' . $key);
        }
    }

    /**
     * Returns a dummy config with valid proxy hosts
     *
     * @param string[]|null $proxyHosts The proxy hosts.
     *
     * @return Config
     */
    private function getDummyConfig(array $proxyHosts = null): Config
    {
        $data = ['forwarded-for' => []];

        if ($proxyHosts !== null && count($proxyHosts)) {
            $data['forwarded-for']['proxyhost'] = $proxyHosts;
        }

        return Config::__set_state(['configFolder' => __DIR__, 'projectFolder' => __DIR__, 'data' => $data]);
    }
}
