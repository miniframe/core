<?php

namespace Miniframe\Middleware;

use Miniframe\Core\Config;
use Miniframe\Core\Request;
use PHPUnit\Framework\TestCase;

class SessionTest extends TestCase
{
    /**
     * Initiates this test.
     */
    public function __construct()
    {
        // @phpstan-ignore-next-line
        call_user_func_array(['parent', '__construct'], func_get_args());

        // Prevent error "Cannot start session when headers already sent"
        ini_set("session.use_cookies", '0');
        ini_set("session.use_only_cookies", '0');
        ini_set("session.cache_limiter", '');
    }

    /**
     * Tests the get, set and commit method
     *
     * @return void
     */
    public function testGetSetCommit(): void
    {
        $session = new Session(
            Request::getActual(),
            Config::__set_state(['configFolder' => __DIR__, 'projectFolder' => __DIR__, 'data' => array()])
        );

        $session->commit();
        $session->set('foo', 'bar');
        $this->assertEquals('bar', $session->get('foo'));
        $session->commit();
    }

    /**
     * Test if we have a session ID
     *
     * @return void
     */
    public function testGetSessionId(): void
    {
        $session = new Session(
            Request::getActual(),
            Config::__set_state(['configFolder' => __DIR__, 'projectFolder' => __DIR__, 'data' => array()])
        );

        // No session started, session ID = null
        $this->assertNull($session->getSessionId());

        // Start session, so we have a session ID
        $session->begin();
        $sessionId = $session->getSessionId();
        $this->assertIsString($sessionId);
        $this->assertMatchesRegularExpression('/[a-z0-9]+/', $sessionId);
        $session->commit();
    }
}
