<?php

namespace Miniframe\Middleware;

use Garrcomm\PHPUnitHelpers\FunctionMock;
use Miniframe\Core\Config;
use Miniframe\Core\Request;
use Miniframe\Core\RequestTest;
use Miniframe\Response\RedirectResponse;
use Miniframe\Response\UnauthorizedResponse;
use PHPUnit\Framework\TestCase;

class BasicAuthenticationTest extends TestCase
{
    /**
     * Basic configuration for testing
     *
     * @var mixed[]
     */
    private $basicConfig = [
        'framework' => [
            'base_href' => '/',
        ],
        'authentication' => [
            'realm' => 'My Secret',
            'user' => [
                'stefan' => '$2y$10$Gp/MVZmhbdC6sJKRutu.JOZXIjpOycbcuYnjOwJ8TJjFDV4slQIVC',
            ],
            'exclude' => [
                '/public',
                '/public/*',
            ],
        ]
    ];

    /**
     * Will be used to emulate the php_sapi_name() result
     *
     * @var string|null
     */
    protected static $phpSapiName = null;

    /**
     * Reset the PHP Server API name before each test to prevent data from a previous test to be active
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        static::$phpSapiName = null;

        // Mocks the php_sapi_name() method which is built in PHP
        FunctionMock::mock('Miniframe\\Core', 'php_sapi_name', function () {
            return static::$phpSapiName;
        });
    }

    /**
     * This method is called after each test.
     *
     * @return void
     */
    protected function tearDown(): void
    {
        FunctionMock::releaseAll();
        parent::tearDown();
    }

    /**
     * Dataprovider for testNoCredentialsSpecified
     *
     * @return array{string, bool}[]
     */
    public function noCredentialsSpecifiedDataProvider(): array
    {
        return [
            // URL                  Should pass authentication
            ['/',                   false],
            ['/private',            false],
            ['/private/verysecret', false],
            ['/private/public',     false],
            ['/public',             true],
            ['/public/subpage',     true],
        ];
    }

    /**
     * Tests what happens when no credentials are specified
     *
     * @param string  $requestUri The requested URL.
     * @param boolean $shouldPass When true, authentication should pass, when false, it should fail.
     *
     * @dataProvider noCredentialsSpecifiedDataProvider
     *
     * @return void
     */
    public function testNoCredentialsSpecified(string $requestUri, bool $shouldPass): void
    {
        $request = new Request(['REQUEST_URI' => $requestUri]);
        $config = $this->getDummyConfig($this->basicConfig);

        if (!$shouldPass) {
            $this->expectException(UnauthorizedResponse::class);
        }
        $middleware = new BasicAuthentication($request, $config);
        if ($shouldPass) {
            $this->assertInstanceOf(BasicAuthentication::class, $middleware);
        }
    }

    /**
     * Tests what happens when the usertable is not configured correctly
     *
     * @return void
     */
    public function testInvalidUserTable(): void
    {
        // This config is invalid; the user table must be an array
        $configArray = $this->basicConfig;
        // @phpstan-ignore-next-line
        $configArray['authentication']['user'] = 'stefan:$2y$10$Gp/MVZmhbdC6sJKRutu.JOZXIjpOycbcuYnjOwJ8TJjFDV4slQIVC';

        $request = new Request(['REQUEST_URI' => '/', 'PHP_AUTH_USER' => 'invalid', 'PHP_AUTH_PW' => 'invalid']);
        $config = $this->getDummyConfig($configArray);

        $this->expectException(\RuntimeException::class);
        $middleware = new BasicAuthentication($request, $config);
    }

    /**
     * Tests what happens when an unknown user is specified
     *
     * @return void
     */
    public function testUnknownUserSpecified(): void
    {
        $request = new Request(['REQUEST_URI' => '/', 'PHP_AUTH_USER' => 'invalid', 'PHP_AUTH_PW' => 'invalid']);
        $config = $this->getDummyConfig($this->basicConfig);

        $this->expectException(UnauthorizedResponse::class);
        $middleware = new BasicAuthentication($request, $config);
    }

    /**
     * Tests what happens when an unknown user is specified
     *
     * @return void
     */
    public function testCalledFromShell(): void
    {
        static::$phpSapiName = 'cli';
        $request = new Request(['REQUEST_URI' => '/']);
        $config = $this->getDummyConfig($this->basicConfig);

        $middleware = new BasicAuthentication($request, $config);
        $this->assertInstanceOf(BasicAuthentication::class, $middleware);
    }

    /**
     * Tests what happens when an invalid password is specified
     *
     * @return void
     */
    public function testInvalidPasswordSpecified(): void
    {
        $request = new Request(['REQUEST_URI' => '/', 'PHP_AUTH_USER' => 'stefan', 'PHP_AUTH_PW' => 'invalid']);
        $config = $this->getDummyConfig($this->basicConfig);

        $this->expectException(UnauthorizedResponse::class);
        $middleware = new BasicAuthentication($request, $config);
    }

    /**
     * Tests what happens with a valid login
     *
     * @return void
     */
    public function testValidLogin(): void
    {
        $request = new Request(['REQUEST_URI' => '/', 'PHP_AUTH_USER' => 'stefan', 'PHP_AUTH_PW' => 'Welcome123']);
        $config = $this->getDummyConfig($this->basicConfig);

        $middleware = new BasicAuthentication($request, $config);
        $this->assertInstanceOf(BasicAuthentication::class, $middleware);
        $this->assertEquals('stefan', $middleware->getUsername());
    }

    /**
     * Tests if the logout returns a redirect without errors/warnings
     *
     * @return void
     */
    public function testLogout(): void
    {
        $request = new Request(['REQUEST_URI' => '/', 'PHP_AUTH_USER' => 'stefan', 'PHP_AUTH_PW' => 'Welcome123']);
        $config = $this->getDummyConfig($this->basicConfig);

        $response = (new BasicAuthentication($request, $config))->logout();
        $this->assertInstanceOf(RedirectResponse::class, $response);
    }

    /**
     * Returns a fictive configuration with specific data
     *
     * @param mixed[] $data The fictive data.
     *
     * @return Config
     */
    private function getDummyConfig(array $data): Config
    {
        return Config::__set_state(['configFolder' => __DIR__, 'projectFolder' => __DIR__, 'data' => $data]);
    }
}
