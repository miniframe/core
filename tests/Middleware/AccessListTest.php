<?php

namespace Miniframe\Middleware;

use Miniframe\Core\Config;
use Miniframe\Core\Request;
use Miniframe\Response\ForbiddenResponse;
use PHPUnit\Framework\TestCase;

class AccessListTest extends TestCase
{
    /**
     * Contains simple test data with IPv4 addresses for wildcard, CIDR and direct matches.
     *
     * @return array<string, array{userIp: string, matchIp: string, match: bool}>
     */
    private function ipv4TestData(): array
    {
        return array(
            'Exact match, success'   => ['userIp' => '192.168.2.230', 'matchIp' => '192.168.2.230',  'match' => true],
            'Exact match, fail'      => ['userIp' => '192.168.3.230', 'matchIp' => '192.168.2.230',  'match' => false],
            'Wildcard match, fail'   => ['userIp' => '192.168.3.230', 'matchIp' => '192.168.2.*',    'match' => false],
            'CIDR match, success'    => ['userIp' => '192.168.2.230', 'matchIp' => '192.168.2.0/24', 'match' => true],
            'CIDR match, fail'       => ['userIp' => '192.168.3.230', 'matchIp' => '192.168.2.0/24', 'match' => false],
            'Hostname to IPv4, success' => ['userIp' => '8.8.8.8',       'matchIp' => 'dns.google',  'match' => true],
            'Hostname to IPv4, fail'    => ['userIp' => '127.0.0.1',     'matchIp' => 'dns.google',  'match' => false],
            'Wildcard match, success'   => ['userIp' => '192.168.2.230', 'matchIp' => '192.168.2.*', 'match' => true],
            'Wildcard match, success 2' => ['userIp' => '10.1.123.45',   'matchIp' => '10.*.*.*',    'match' => true],
        );
    }

    /**
     * Contains simple test data with IPv6 addresses for bitwise, CIDR and direct matches.
     *
     * @return array<string, array{userIp: string, matchIp: string, match: bool}>
     */
    private function ipv6TestData(): array
    {
        return array(
            'Exact match, success'         => ['userIp' => '::1', 'matchIp' => '::1',             'match' => true],
            'Exact match, fail'            => ['userIp' => '::2', 'matchIp' => '::1',             'match' => false],
            'Bitwise exact match, success' => ['userIp' => '::1', 'matchIp' => '0:0:0:0:0:0:0:1', 'match' => true],
            'Bitwise exact match, fail'    => ['userIp' => '::2', 'matchIp' => '0:0:0:0:0:0:0:1', 'match' => false],

            'Hostname to IPv6, success' => [
                'userIp' => '2001:4860:4860::8844',
                'matchIp' => 'dns.google',
                'match' => true
            ],

            'Hostname to IPv6, fail' => [
                'userIp' => '::2',
                'matchIp' => 'dns.google',
                'match' => false
            ],

            'CIDR match, success' => [
                'userIp'   => '21DA:00D3:0000:2F3B:02AC:00FF:FE28:9C5A',
                'matchIp' => '21DA:00D3:0000:2F3B::/64',
                'match'    => true
            ],
            'CIDR match, fail' => [
                'userIp'   => '22DA:00D3:0000:2F3B:02AC:00FF:FE28:9C5A',
                'matchIp' => '21DA:00D3:0000:2F3B::/64',
                'match'    => false
            ],
        );
    }

    /**
     * More complex tests with different orders and both allow/deny lists.
     *
     * @return array<string, array{order: string, allow: string[], deny: string[], allows: array<string, bool>}>
     */
    private function orderTestData(): array
    {
        $tests = array();

        $tests['allow all, deny 192.168.2.100'] = [
            'order'   => 'allow,deny',
            'allow'   => ['All'],
            'deny'    => ['192.168.2.100'],
            'allows'  => [
                '192.168.2.100' => false, // Match both Allow & Deny: Final match controls: Denied
                '192.168.2.1' => true,    // Match Allow only: Request allowed
            ],
        ];

        $tests['deny 192.168.2.100, allow all'] = [
            'order'   => 'deny,allow',
            'allow'   => ['All'],
            'deny'    => ['192.168.2.100'],
            'allows'  => [
                '192.168.2.100' => true, // Match both Allow & Deny: Final match controls: Allowed
                '192.168.2.1' => true,   // Match Allow only: Request allowed
            ],
        ];

        $tests['deny all, allow 192.168.2.100'] = [
            'order'   => 'deny,allow',
            'allow'   => ['192.168.2.100'],
            'deny'    => ['All'],
            'allows'  => [
                '192.168.2.100' => true, // Match both Allow & Deny: Final match controls: Allowed
                '192.168.2.1' => false,  // Match Deny only: Request denied
            ],
        ];

        $tests['allow 192.168.2.100, deny all'] = [
            'order'   => 'allow,deny',
            'allow'   => ['192.168.2.100'],
            'deny'    => ['All'],
            'allows'  => [
                '192.168.2.100' => false, // Match both Allow & Deny: Final match controls: Denied
                '192.168.2.1' => false,   // Match Deny only: Request denied
            ],
        ];

        $tests['allow 192.168.2.100, deny 192.168.2.*'] = [
            'order'   => 'allow,deny',
            'allow'   => ['192.168.2.100', '172.16.0.1'],
            'deny'    => ['192.168.2.*'],
            'allows'  => [
                '192.168.2.100' => false, // Match both Allow & Deny: Final match controls: Denied
                '192.168.2.1' => false,   // Match Deny only: Request denied
                '10.0.0.1' => false,      // No match: Default to second directive: Denied
                '172.16.0.1' => true,     // Match Allow only: Request allowed
            ],
        ];

        $tests['deny 192.168.2.*, allow 192.168.2.100'] = [
            'order'   => 'deny,allow',
            'allow'   => ['192.168.2.100', '172.16.0.1'],
            'deny'    => ['192.168.2.*'],
            'allows'  => [
                '192.168.2.100' => true,  // Match both Allow & Deny: Final match controls: Allowed
                '192.168.2.1' => false,   // Match Deny only: Request denied
                '10.0.0.1' => true,       // No match: Default to second directive: Allowed
                '172.16.0.1' => true,     // Match Allow only: Request allowed
            ],
        ];

        return $tests;
    }

    /**
     * Combine all test data into one large array with tests
     *
     * @return array<
     *   string,
     *   array{
     *     order: string|null,
     *     allow: string[]|null,
     *     deny: string[]|null,
     *     testIp: string,
     *     isAllowed: bool
     *   }
     * >
     */
    public function accessListDataProvider(): array
    {
        // Splits the Order tests to make a line for each allow-check
        $return = array();
        foreach ($this->orderTestData() as $name => $data) {
            foreach ($data['allows'] as $ip => $isAllowed) {
                $return[$name . ' - ' . $ip] = [
                    'order'     => $data['order'],
                    'allow'     => $data['allow'],
                    'deny'      => $data['deny'],
                    'testIp'    => $ip,
                    'isAllowed' => $isAllowed
                ];
            }
        }

        // Add IPv4 & IPv6 simple testcases, test them with `allow` and `deny`
        foreach (['IPv4' => $this->ipv4TestData(), 'IPv6' => $this->ipv6TestData()] as $ipv => $tests) {
            foreach ($tests as $name => $data) {
                $return[$ipv . ' ' . $name . ' allow'] = [
                    'order'     => null,
                    'allow'     => [$data['matchIp']],
                    'deny'      => null,
                    'testIp'    => $data['userIp'],
                    'isAllowed' => $data['match'],
                ];
                $return[$ipv . ' ' . $name . ' deny'] = [
                    'order'     => null,
                    'allow'     => null,
                    'deny'      => [$data['matchIp']],
                    'testIp'    => $data['userIp'],
                    'isAllowed' => !$data['match'],
                ];
            }
        }

        return $return;
    }

    /**
     * Tests if an IP address is allowed, based on a specific order, allow and deny list
     *
     * @param string|null   $order     The order.
     * @param string[]|null $allow     Allow list.
     * @param string[]|null $deny      Deny list.
     * @param string        $testIp    IP to test.
     * @param boolean       $isAllowed When true, IP should have access, otherwise false.
     *
     * @return void
     *
     * @dataProvider accessListDataProvider
     */
    public function testAccessList(?string $order, ?array $allow, ?array $deny, string $testIp, bool $isAllowed): void
    {
        if (!$isAllowed) {
            $this->expectException(ForbiddenResponse::class);
        }
        $middleware = new AccessList($this->getDummyRequest($testIp), $this->getDummyConfig($allow, $deny, $order));
        if ($isAllowed) {
            $this->assertInstanceOf(AccessList::class, $middleware);
        }
    }

    /**
     * Tests what happens when the client has no IP (should continue)
     *
     * @return void
     */
    public function testNoIp(): void
    {
        $middleware = new AccessList($this->getDummyRequest(null), $this->getDummyConfig(['127.0.0.1'], null));
        $this->assertInstanceOf(AccessList::class, $middleware);
    }

    /**
     * Tests what happens when the allow list is a string, not an array
     *
     * @return void
     */
    public function testAllowAsString(): void
    {
        $this->expectException(\RuntimeException::class);
        new AccessList($this->getDummyRequest('127.0.0.1'), $this->getDummyConfig('127.0.0.1', null));
    }

    /**
     * Tests what happens when the deny list is a string, not an array
     *
     * @return void
     */
    public function testDenyAsString(): void
    {
        $this->expectException(\RuntimeException::class);
        new AccessList($this->getDummyRequest('127.0.0.1'), $this->getDummyConfig(null, '127.0.0.1'));
    }

    /**
     * Tests what happens when the deny list is a string, not an array
     *
     * @return void
     */
    public function testInvalidIp(): void
    {
        $this->expectException(\RuntimeException::class);
        new AccessList($this->getDummyRequest('abc'), $this->getDummyConfig(['127.0.0.1'], null));
    }

    /**
     * Tests what if we specify an invalid order
     *
     * @return void
     */
    public function testInvalidOrder(): void
    {
        $this->expectException(\RuntimeException::class);
        new AccessList($this->getDummyRequest('127.0.0.1'), $this->getDummyConfig(null, null, 'invalid'));
    }

    /**
     * Tests what if we specify an invalid order
     *
     * @return void
     */
    public function testInvalidOrderArray(): void
    {
        $this->expectException(\RuntimeException::class);
        new AccessList($this->getDummyRequest('127.0.0.1'), $this->getDummyConfig(null, null, ['allow', 'deny']));
    }

    /**
     * Returns a dummy config with specific allow/deny entries
     *
     * @param mixed                $allow When null, the value will be ignored.
     * @param mixed                $deny  When null, the value will be ignored.
     * @param string[]|string|null $order The order config flag.
     *
     * @return Config
     */
    private function getDummyConfig($allow, $deny, $order = null): Config
    {
        $data = ['accesslist' => []];

        if ($allow !== null) {
            $data['accesslist']['allow'] = $allow;
        }
        if ($deny !== null) {
            $data['accesslist']['deny'] = $deny;
        }
        if ($order !== null) {
            $data['accesslist']['order'] = $order;
        }

        return Config::__set_state(['configFolder' => __DIR__, 'projectFolder' => __DIR__, 'data' => $data]);
    }

    /**
     * Returns a Request object, requesting the root from a specific IP address
     *
     * @param string|null $ipAddress The client IP address.
     *
     * @return Request
     */
    private function getDummyRequest(?string $ipAddress): Request
    {
        if ($ipAddress) {
            return new Request(['REQUEST_URI' => '/', 'REMOTE_ADDR' => $ipAddress]);
        } else {
            return new Request(['REQUEST_URI' => '/']);
        }
    }
}
