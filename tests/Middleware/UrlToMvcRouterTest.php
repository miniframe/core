<?php

namespace Miniframe\Middleware;

use App\Controller\Foo;
use App\Controller\FooBar;
use App\Controller\Index;
use Miniframe\Core\Config;
use Miniframe\Core\Request;
use PHPUnit\Framework\TestCase;

class UrlToMvcRouterTest extends TestCase
{
    /**
     * Data provider for the requestToMvc test
     *
     * @return array{Request, callable|null}[]
     */
    public function requestToMvcDataProviderRoot(): array
    {
        return $this->requestToMvcDataProviderHelper('');
    }

    /**
     * Data provider for the requestToMvc test with prefix
     *
     * @return array{Request, callable|null}[]
     */
    public function requestToMvcDataProviderPrefixed(): array
    {
        return $this->requestToMvcDataProviderHelper('/public');
    }

    /**
     * Returns data for testing with URLs with a specific prefix.
     *
     * @param string $prefix The prefix.
     *
     * @return array{Request, callable|null}[]
     */
    private function requestToMvcDataProviderHelper(string $prefix): array
    {
        return array(
            [new Request(['REQUEST_URI' => $prefix . '/']),                [Index::class, 'main']],
            [new Request(['REQUEST_URI' => $prefix . '/index']),           [Index::class, 'main']],
            [new Request(['REQUEST_URI' => $prefix . '/index/main']),      [Index::class, 'main']],
            [new Request(['REQUEST_URI' => $prefix . '/foo/bar']),         [Foo::class,   'bar']],
            [new Request(['REQUEST_URI' => $prefix . '/foo_bar/bar_baz']), [FooBar::class, 'barBaz']],
            [new Request(['REQUEST_URI' => $prefix . '/fooBar/barBaz']),   [FooBar::class, 'barBaz']],
            [new Request(['REQUEST_URI' => $prefix . '/404']),             null], // Controller not found
            [new Request(['REQUEST_URI' => $prefix . '/index/404']),       null], // Method not found
        );
    }

    /**
     * Tests if the getRouters returns at least one callable
     *
     * @return void
     */
    public function testGetRouters(): void
    {
        $middleware = new UrlToMvcRouter(Request::getActual(), $this->getDummyConfig());
        $routers = $middleware->getRouters();
        $this->assertGreaterThan(0, count($routers));
        foreach ($routers as $router) {
            $this->assertIsCallable($router);
        }
    }

    /**
     * Checks if routes are handled correctly
     *
     * @param Request $request Request object.
     * @param mixed   $result  Expected result.
     *
     * @return void
     *
     * @dataProvider requestToMvcDataProviderRoot
     */
    public function testRequestToMvc(Request $request, $result): void
    {
        $config = $this->getDummyConfig();

        // Make sure the first key is an object with the same Request object and configuration
        if (is_array($result) && is_string($result[0])) {
            $result[0] = new $result[0]($request, $config);
        }

        // Validate if we get the correct result back
        $middleware = new UrlToMvcRouter($request, $config);
        $this->assertEquals($result, $middleware->requestToMvc());
    }

    /**
     * Checks if routes are handled correctly
     *
     * @param Request $request Request object.
     * @param mixed   $result  Expected result.
     *
     * @return void
     *
     * @dataProvider requestToMvcDataProviderPrefixed
     */
    public function testRequestToMvcSubpath(Request $request, $result): void
    {
        $config = $this->getDummyConfig('/public');

        // Make sure the first key is an object with the same Request object and configuration
        if (is_array($result) && is_string($result[0])) {
            $result[0] = new $result[0]($request, $config);
        }

        // Validate if we get the correct result back
        $middleware = new UrlToMvcRouter($request, $config);
        $this->assertEquals($result, $middleware->requestToMvc());
    }

    /**
     * The controller should always extend AbstractController.
     *
     * @return void
     */
    public function testInvalidController(): void
    {
        // Mock config and simulate request
        $config = $this->getDummyConfig();
        $request = new Request(['REQUEST_URI' => '/bar/main']);

        // Validate if we get the correct result back
        $middleware = new UrlToMvcRouter($request, $config);
        $this->expectException(\RuntimeException::class);
        $middleware->requestToMvc();
    }

    /**
     * Returns a fictive configuration with a specific base_href
     *
     * @param string $baseHref The base href.
     *
     * @return Config
     */
    private function getDummyConfig(string $baseHref = '/'): Config
    {
        return Config::__set_state(['configFolder' => __DIR__, 'projectFolder' => __DIR__, 'data' => [
            'framework' => ['base_href' => $baseHref]]
        ]);
    }
}
