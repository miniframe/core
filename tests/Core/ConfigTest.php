<?php

namespace Miniframe\Core;

use PHPUnit\Framework\TestCase;

class ConfigTest extends TestCase
{
    /**
     * Path to the test ini files, which are created in setUpBeforeClass and destroyed in tearDownAfterClass
     *
     * @var string
     */
    private static $iniFolder;

    /**
     * Root folder of the unit tests
     *
     * @var string
     */
    private static $testRootFolder = __DIR__ . '/../';

    /**
     * Returns the contents for the main test ini file
     *
     * @return string
     */
    private static function testMainIniFile(): string
    {
        return '[framework]' . PHP_EOL
            . 'base_href = /public' . PHP_EOL
            . PHP_EOL
            . '[section]' . PHP_EOL
            . 'string_with_space = " whitespace"' . PHP_EOL
            . 'bool = on' . PHP_EOL
            . 'bool_off = off' . PHP_EOL
            . 'bool_as_string = "on"' . PHP_EOL
            . 'nullable = null' . PHP_EOL
            . 'nullable_as_string = "null"' . PHP_EOL
            . 'array[] = foo' . PHP_EOL
            . 'array[] = bar' . PHP_EOL
            . 'associative_array[foo] = abc' . PHP_EOL
            . 'associative_array[bar] = def' . PHP_EOL
            . 'mainsub_string = foo' . PHP_EOL
            . 'mainsub_array[] = foo' . PHP_EOL
            . 'mainsub_associative_array[foo] = abc' . PHP_EOL
            . 'mainsub_associative_array[bar] = def' . PHP_EOL
            . 'converted = foo' . PHP_EOL
            . PHP_EOL
            . '[paths]' . PHP_EOL
            . 'bin_folder = ../bin' . PHP_EOL
            . 'tmp_folder = /tmp' . PHP_EOL
            . 'tmp_folder_win = c:\\temp' . PHP_EOL
            . 'testfiles[] = Core/ConfigTest.php' . PHP_EOL
            . 'testfiles[] = Core/RegistryTest.php' . PHP_EOL
            . 'testfiles[] = Core/RequestTest.php' . PHP_EOL
            . 'testfiles[] = Core/ResponseTest.php' . PHP_EOL
            ;
    }

    /**
     * Returns the contents for the sub test ini file
     *
     * @return string
     */
    private static function testSubIniFile(): string
    {
        return '[section]' . PHP_EOL
            . 'mainsub_string = bar' . PHP_EOL
            . 'mainsub_array[] = bar' . PHP_EOL
            . 'mainsub_associative_array[bar] = ghi' . PHP_EOL
            . 'mainsub_associative_array[baz] = jkl' . PHP_EOL
            . 'converted[] = bar' . PHP_EOL
            ;
    }

    /**
     * Dataprovider containing tests for getting several variable types
     *
     * @return array<string, array{string, string, mixed}>
     */
    public function getDataProvider(): array
    {
        return [
            'Simple string' => ['framework', 'base_href', '/public'],
            'String with prepending whitespace' => ['section', 'string_with_space', ' whitespace'],
            'Array' => ['section', 'array', ['foo', 'bar']],
            'Associative array' => ['section', 'associative_array', ['foo' => 'abc', 'bar' => 'def']],
            'Boolean' => ['section', 'bool', true],
            'Boolean off' => ['section', 'bool_off', false],
            'Boolean as string' => ['section', 'bool_as_string', 'on'],
            'Null' => ['section', 'nullable', null],
            'Null as string' => ['section', 'nullable_as_string', 'null'],
            'Main and sub combined string' => ['section', 'mainsub_string', 'bar'],
            'Two configs, one array' => ['section', 'mainsub_array', ['foo', 'bar']],
            'Complex config array merge' => [
                'section', 'mainsub_associative_array',
                ['foo' => 'abc', 'bar' => 'ghi', 'baz' => 'jkl']
            ],
            'Convert string to array' => [
                'section', 'converted',
                ['foo', 'bar']
            ]
        ];
    }

    /**
     * Dataprovider containing tests for testing if we have specific config variables
     *
     * @return array<string, array{string, string, bool}>
     */
    public function hasDataProvider(): array
    {
        $return = [
            'Non existent section' => ['nonExistentSection', 'key', false],
            'Non existent key' => ['framework', 'nonExistentKey', false],
        ];

        // Add all values from the getDataProvider() with 'true' as result
        foreach ($this->getDataProvider() as $description => $data) {
            $return[$description] = [$data[0], $data[1], true];
        }

        return $return;
    }

    /**
     * This method is called before the first test of this test class is run and prepares a temporary config folder.
     *
     * @return void
     */
    public static function setUpBeforeClass(): void
    {
        parent::setUpBeforeClass();
        $iniFolder = tempnam(sys_get_temp_dir(), 'unittest');
        if ($iniFolder === false) {
            throw new \RuntimeException('Could not create temporary ini files');
        }
        self::$iniFolder = $iniFolder;
        if (file_exists(self::$iniFolder)) {
            unlink(self::$iniFolder);
        }
        mkdir(self::$iniFolder, 0777, true);
        file_put_contents(self::$iniFolder . '/main.ini', self::testMainIniFile());
        file_put_contents(self::$iniFolder . '/sub.ini', self::testSubIniFile());
        if (!file_exists(self::$iniFolder . '/main.ini') || !file_exists(self::$iniFolder . '/sub.ini')) {
            throw new \RuntimeException('Could not create temporary ini files');
        }
    }

    /**
     * This method is called after the last test of this test class is run and cleans up the temporary config folder.
     *
     * @return void
     */
    public static function tearDownAfterClass(): void
    {
        parent::tearDownAfterClass();
        unlink(self::$iniFolder . '/main.ini');
        unlink(self::$iniFolder . '/sub.ini');
        rmdir(self::$iniFolder);
    }

    /**
     * Tests if we get the correct value back
     *
     * @param string $section The config section.
     * @param string $key     The config key.
     * @param mixed  $value   The expected value.
     *
     * @return void
     *
     * @dataProvider getDataProvider
     */
    public function testGet(string $section, string $key, $value): void
    {
        $config = new Config(self::$iniFolder, self::$testRootFolder);
        $this->assertEquals($value, $config->get($section, $key));
    }

    /**
     * Tests if we values are defined
     *
     * @param string  $section The config section.
     * @param string  $key     The config key.
     * @param boolean $result  Whether or not the combination should exist.
     *
     * @return void
     *
     * @dataProvider hasDataProvider
     */
    public function testHas(string $section, string $key, bool $result): void
    {
        $config = new Config(self::$iniFolder, self::$testRootFolder);
        $this->assertEquals($result, $config->has($section, $key));
    }

    /**
     * Tests what happens when we request an unknown config section
     *
     * @return void
     */
    public function testGetUnknownSection(): void
    {
        $config = new Config(self::$iniFolder, self::$testRootFolder);
        $this->expectException(\RuntimeException::class);
        $config->get('nonExistent', 'nonExistent');
    }

    /**
     * Tests what happens when we request an unknown config value
     *
     * @return void
     */
    public function testGetUnknownKey(): void
    {
        $config = new Config(self::$iniFolder, self::$testRootFolder);
        $this->expectException(\RuntimeException::class);
        $config->get('framework', 'nonExistent');
    }
    /**
     * Tests what happens when we request a path from a non-string
     *
     * @return void
     */
    public function testInvalidPathType(): void
    {
        $config = new Config(self::$iniFolder, self::$testRootFolder);
        $this->expectException(\RuntimeException::class);
        $config->getPath('section', 'bool');
    }

    /**
     * Tests getting a path from the config
     *
     * @return void
     */
    public function testGetPath(): void
    {
        $config = new Config(self::$iniFolder, self::$testRootFolder);
        $config_bin_folder = $config->getPath('paths', 'bin_folder');
        $relative_bin_folder = realpath(__DIR__ . '/../../bin') . DIRECTORY_SEPARATOR;
        $this->assertEquals($relative_bin_folder, $config_bin_folder);
    }

    /**
     * Tests getting a path from the config
     *
     * @return void
     */
    public function testAbsoluteGetPaths(): void
    {
        $config = new Config(self::$iniFolder, self::$testRootFolder);
        $linux_tmp_folder = $config->getPath('paths', 'tmp_folder');
        $this->assertEquals('/tmp', $linux_tmp_folder);
        $windows_tmp_folder = $config->getPath('paths', 'tmp_folder_win');
        $this->assertEquals('c:\\temp', $windows_tmp_folder);
    }

    /**
     * Tests getting multiple paths from the config
     *
     * @return void
     */
    public function testGetPathArray(): void
    {
        $config = new Config(self::$iniFolder, self::$testRootFolder);
        $testfiles = $config->getPath('paths', 'testfiles');
        $this->assertIsArray($testfiles);
        $this->assertEquals(4, count($testfiles));
        foreach ($testfiles as $testfile) {
            $this->assertIsString($testfile);
            $this->assertFileExists($testfile);
        }
    }

    /**
     * Tests the exception for an invalid config folder
     *
     * @return void
     */
    public function testInvalidConfigFolder(): void
    {
        $this->expectException(\RuntimeException::class);
        new Config(__DIR__ . '/invalid_folder', __DIR__);
    }

    /**
     * Tests the exception for an invalid project folder
     *
     * @return void
     */
    public function testInvalidProjectFolder(): void
    {
        $this->expectException(\RuntimeException::class);
        new Config(__DIR__, __DIR__ . '/invalid_folder');
    }

    /**
     * Test __set_state with valid state
     *
     * @return void
     */
    public function testSetState(): void
    {
        $config = Config::__set_state([
            'configFolder' => self::$iniFolder, 'projectFolder' => self::$testRootFolder, 'data' => []
        ]);
        $this->assertInstanceOf(Config::class, $config);
    }

    /**
     * Test __set_state with invalid state
     *
     * @return void
     */
    public function testInvalidState(): void
    {
        $this->expectException(\RuntimeException::class);
        Config::__set_state([]);
    }
}
