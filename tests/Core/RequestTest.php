<?php

namespace Miniframe\Core;

use Garrcomm\PHPUnitHelpers\FunctionMock;
use PHPUnit\Framework\TestCase;

class RequestTest extends TestCase
{
    /**
     * Will be used to emulate the php_sapi_name() result
     *
     * @var string|null
     */
    protected static $phpSapiName = null;

    /**
     * Reset the PHP Server API name before each test to prevent data from a previous test to be active
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        static::$phpSapiName = null;

        // Mocks the php_sapi_name() method which is built in PHP
        FunctionMock::mock('Miniframe\\Core', 'php_sapi_name', function () {
            return static::$phpSapiName;
        });
    }

    /**
     * This method is called after each test.
     *
     * @return void
     */
    protected function tearDown(): void
    {
        FunctionMock::releaseAll();
        parent::tearDown();
    }

    /**
     * Data provider for several tests
     *
     * @return array<
     *   string,
     *   array{
     *     request: Request,
     *     path: string[],
     *     query: array<string, string|string[]>,
     *     server: array<string, string|string[]>,
     *     payload: string|null,
     *     php_sapi_name: string
     *   }
     * >
     */
    public function requestDataProvider(): array
    {
        return array(
            'Simple web request 1' => [
                'request' => new Request(['REQUEST_URI' => '/foo/bar', 'HTTP_USER_AGENT' => 'cURL/Or/Whatever']),
                'path'    => ['foo', 'bar'],
                'query'   => [],
                'server'  => ['REQUEST_URI' => '/foo/bar', 'HTTP_USER_AGENT' => 'cURL/Or/Whatever'],
                'payload' => null,
                'php_sapi_name' => 'cgi',
            ],
            'Simple web request 2' => [
                'request' => new Request(['REQUEST_URI' => '/foo/bar?page=1'], ['page' => '1']),
                'path'    => ['foo', 'bar'],
                'query'   => ['page' => '1'],
                'server'  => ['REQUEST_URI' => '/foo/bar?page=1'],
                'payload' => null,
                'php_sapi_name' => 'cgi',
            ],
            'Simple web request 3' => [
                'request' => new Request(['REQUEST_URI' => '/foo/bar/?page=1'], ['page' => '1']),
                'path'    => ['foo', 'bar'],
                'query'   => ['page' => '1'],
                'server'  => ['REQUEST_URI' => '/foo/bar/?page=1'],
                'payload' => null,
                'php_sapi_name' => 'cgi',
            ],
            'Advanced web request' => [
                'request' => new Request(['REQUEST_URI' => '/foo/bar?page=1&seq=ASC'], ['page' => '1', 'seq' => 'ASC']),
                'path'    => ['foo', 'bar'],
                'query'   => ['page' => '1', 'seq' => 'ASC'],
                'server'  => ['REQUEST_URI' => '/foo/bar?page=1&seq=ASC'],
                'payload' => null,
                'php_sapi_name' => 'cgi',
            ],
            'Advanced POST request' => [
                'request' => new Request(['REQUEST_URI' => '/'], [], [], [], "['foo', 'bar']"),
                'path'    => [],
                'query'   => [],
                'server'  => ['REQUEST_URI' => '/'],
                'payload' => "['foo', 'bar']",
                'php_sapi_name' => 'cgi',
            ],
            'Simple shell test 1' => [
                'request' => new Request(['argv' => explode(' ', 'index.php foo bar --page 1')]),
                'path'    => ['foo', 'bar'],
                'query'   => ['page' => '1'],
                'server'  => ['argv' => explode(' ', 'index.php foo bar --page 1')],
                'payload' => null,
                'php_sapi_name' => 'cli',
            ],
            'Simple shell test 2' => [
                'request' => new Request(['argv' => explode(' ', 'index.php foo bar --page=1 --seq=ASC')]),
                'path'    => ['foo', 'bar'],
                'query'   => ['page' => '1', 'seq' => 'ASC'],
                'server'  => ['argv' => explode(' ', 'index.php foo bar --page=1 --seq=ASC')],
                'payload' => null,
                'php_sapi_name' => 'cli',
            ],
            'Advanced shell test' => [
                'request' => new Request(['argv' => explode(' ', 'index.php foo bar -a 1 -b 2 -a 2 -c 3 -d')]),
                'path'    => ['foo', 'bar'],
                'query'   => ['a' => ['1', '2'], 'b' => '2', 'c' => '3', 'd' => 'true'],
                'server'  => ['argv' => explode(' ', 'index.php foo bar -a 1 -b 2 -a 2 -c 3 -d')],
                'payload' => null,
                'php_sapi_name' => 'cli',
            ],
            'Large array shell test' => [
                'request' => new Request(['argv' => explode(' ', 'index.php foo bar -a 1 -a 2 -a 3 -b')]),
                'path'    => ['foo', 'bar'],
                'query'   => ['a' => ['1', '2', '3'], 'b' => 'true'],
                'server'  => ['argv' => explode(' ', 'index.php foo bar -a 1 -a 2 -a 3 -b')],
                'payload' => null,
                'php_sapi_name' => 'cli',
            ],
        );
    }

    /**
     * Test for the Request->isHttpsRequest() method
     *
     * @return void
     */
    public function testIsHttpsRequest(): void
    {
        // HTTPS: on
        $secureRequest = new Request(['HTTPS' => 'ON', 'REQUEST_URI' => '/']);
        $this->assertTrue($secureRequest->isHttpsRequest(), 'Request->isHttps() did not return expected true');
        // HTTPS: off
        $insecureRequest = new Request(['REQUEST_URI' => '/']);
        $this->assertFalse($insecureRequest->isHttpsRequest(), 'Request->isHttps() did not return expected false');
    }

    /**
     * Test for the Request->isShellRequest() method
     *
     * @return void
     */
    public function testIsShellRequest(): void
    {
        static::$phpSapiName = 'cli';
        // Shell request: on
        $shellRequest = new Request(['argv' => ['index.php', '--help']]);
        $this->assertTrue($shellRequest->isShellRequest(), 'Request->isShellRequest() did not return expected true');
        $this->assertEquals('index.php', $shellRequest->getPath(-1));
        // Shell request: off
        static::$phpSapiName = 'cgi';
        $shellRequest = new Request(['REQUEST_URI' => '/foo/bar']);
        $this->assertFalse($shellRequest->isShellRequest(), 'Request->isShellRequest() did not return expected false');
    }

    /**
     * Test for the Request->getPath() method
     *
     * @param Request  $request The virtual request.
     * @param string[] $path    The path it should contain.
     *
     * @return void
     *
     * @dataProvider requestDataProvider
     */
    public function testGetPath(Request $request, array $path): void
    {
        $index = 0;
        $this->assertEquals($path, $request->getPath());
        foreach ($path as $index => $value) {
            $this->assertEquals($value, $request->getPath($index));
        }
        $this->assertNull($request->getPath(++$index));
    }

    /**
     * Test for the Request->getRequest() method
     *
     * @param Request             $request       The virtual request.
     * @param string[]            $path          Not used for this test.
     * @param string[]|string[][] $request_data  The request data it should contain.
     * @param string[]|string[][] $server_data   The server data it should contain.
     * @param string|null         $payload       The post payload it should contain.
     * @param string              $php_sapi_name PHP server API name.
     *
     * @return void
     *
     * @dataProvider requestDataProvider
     */
    public function testGetRequest(
        Request $request,
        array $path,
        array $request_data,
        array $server_data,
        ?string $payload,
        string $php_sapi_name
    ): void {
        static::$phpSapiName = $php_sapi_name;
        // Verify a complete request of all data
        $this->assertEquals($request_data, $request->getRequest());
        // Verify a request of each key separately
        foreach ($request_data as $key => $value) {
            $this->assertEquals($value, $request->getRequest($key));
        }
    }

    /**
     * Test for the Request->getServer() method
     *
     * @param Request             $request       The virtual request.
     * @param string[]            $path          Not used for this test.
     * @param string[]|string[][] $request_data  The request data it should contain.
     * @param string[]|string[][] $server_data   The server data it should contain.
     * @param string|null         $payload       The post payload it should contain.
     * @param string              $php_sapi_name PHP server API name.
     *
     * @return void
     *
     * @dataProvider requestDataProvider
     */
    public function testGetServer(
        Request $request,
        array $path,
        array $request_data,
        array $server_data,
        ?string $payload,
        string $php_sapi_name
    ): void {
        static::$phpSapiName = $php_sapi_name;
        // Verify a complete request of all data
        $this->assertEquals($server_data, $request->getServer());
        // Verify a request of each key separately
        foreach ($server_data as $key => $value) {
            $this->assertEquals($value, $request->getServer($key));
        }
    }
    /**
     * Test for the Request->getPayload() method
     *
     * @param Request             $request       The virtual request.
     * @param string[]            $path          Not used for this test.
     * @param string[]|string[][] $request_data  The request data it should contain.
     * @param string[]|string[][] $server_data   The server data it should contain.
     * @param string|null         $payload       The post payload it should contain.
     * @param string              $php_sapi_name PHP server API name.
     *
     * @return void
     *
     * @dataProvider requestDataProvider
     */
    public function testGetPayload(
        Request $request,
        array $path,
        array $request_data,
        array $server_data,
        ?string $payload,
        string $php_sapi_name
    ): void {
        static::$phpSapiName = $php_sapi_name;
        // Verify a complete request of all data
        $this->assertEquals($payload, $request->getPayload());
    }

    /**
     * Test for the Request->getRequest() method
     *
     * @return void
     */
    public function testGetPost(): void
    {
        // Simulated post data
        $post_data = [
            'page' => '1',
            'seq'  => 'ASC',
            'what' => ['foo', 'bar'],
        ];

        // Builds the POST request based on the test data
        $request = new Request(['REQUEST_URI' => '/', 'REQUEST_METHOD' => 'POST'], [], $post_data);

        // Verify a complete request of all data
        $this->assertEquals($post_data, $request->getPost());
        // Verify a request of each key separately
        foreach ($post_data as $key => $value) {
            $this->assertEquals($value, $request->getPost($key));
        }
    }

    /**
     * Test for the Request::getActual() method
     *
     * @return void
     */
    public function testGetActual(): void
    {
        $this->assertInstanceOf(Request::class, Request::getActual());
    }

    /**
     * Test for the Request::__set_state() method {@see var_export}
     *
     * @param Request $request The virtual request.
     *
     * @return void
     *
     * @dataProvider requestDataProvider
     */
    public function testSetState(Request $request): void
    {
        $serialized = var_export($request, true);
        $restored = eval('return ' . $serialized . ';');
        $this->assertEquals($request, $restored);
    }

    /**
     * A request must be filled
     *
     * @return void
     */
    public function testInvalidRequest(): void
    {
        $this->expectException(\RuntimeException::class);
        $request = new Request();
    }
}
