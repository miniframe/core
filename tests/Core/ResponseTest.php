<?php

namespace Miniframe\Core;

use PHPUnit\Framework\TestCase;

class ResponseTest extends TestCase
{
    /**
     * Test for the Response::getExitCode method
     *
     * @return void
     */
    public function testGetExitCode(): void
    {
        $exitCode = 1;
        $response = new Response('foo bar', $exitCode);
        $this->assertEquals($exitCode, $response->getExitCode());
    }

    /**
     * Test for the Response::render method
     *
     * @return void
     */
    public function testRender(): void
    {
        $text = 'foo bar';
        $response = new Response($text);
        $this->assertEquals($text, $response->render());
    }

    /**
     * Test for the Response::setExitCode method
     *
     * @return void
     */
    public function testSetExitCode(): void
    {
        $exitCode = 2;
        $response = new Response('foo bar');
        $response->setExitCode($exitCode);
        $this->assertEquals($exitCode, $response->getExitCode());
    }

    /**
     * Test for the Response::getResponseCode method
     *
     * @return void
     */
    public function testGetResponseCode(): void
    {
        $responseCode = 200; // 200 should be the default!
        $response = new Response('foo bar');
        $this->assertEquals($responseCode, $response->getResponseCode());
    }

    /**
     * Test for the Response::setResponseCode method
     *
     * @return void
     */
    public function testSetResponseCode(): void
    {
        $responseCode = 500;
        $response = new Response('foo bar');
        $response->setResponseCode($responseCode);
        $this->assertEquals($responseCode, $response->getResponseCode());
    }

    /**
     * Test to see if we can throw a Response object
     *
     * @return void
     */
    public function testThrowable(): void
    {
        $response = new Response('Not found');
        $response->setResponseCode(404);
        $this->expectException(Response::class);
        throw $response;
    }

    /**
     * Test for the Response::setHeader and Response::getHeaders methods
     *
     * @return void
     */
    public function testSetGetHeader(): void
    {
        $expected = array(['header' => 'Location: https://www.google.com/', 'replace' => true]);

        $response = new Response('Moved permanently');
        $response->addHeader('Location: https://www.google.com/');
        $this->assertEquals($expected, $response->getHeaders());
    }

    /**
     * Test for the defining multiple headers with the Response::setHeader and Response::getHeaders methods
     *
     * @return void
     */
    public function testSetMultipleHeaders(): void
    {
        $expected = array(
            ['header' => 'WWW-Authenticate: Negotiate', 'replace' => true],
            ['header' => 'WWW-Authenticate: NTLM', 'replace' => false],
        );

        $response = new Response('Foo bar');
        $response->addHeader('WWW-Authenticate: Negotiate');
        $response->addHeader('WWW-Authenticate: NTLM', false);
        $this->assertEquals($expected, $response->getHeaders());
    }
}
