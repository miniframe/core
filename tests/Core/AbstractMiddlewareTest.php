<?php

namespace Miniframe\Core;

use PHPUnit\Framework\TestCase;

/**
 * The abstract middleware class contains some methods that should return defaults.
 *
 * That needs to be tested though, failing in returning the defaults could cause unexpected behaviour.
 */
class AbstractMiddlewareTest extends TestCase
{
    /**
     * Creates a dummy class, extending the AbstractMiddleware class
     *
     * @return AbstractMiddleware
     */
    private function getDummyMiddleware(): AbstractMiddleware
    {
        return new class () extends AbstractMiddleware {
            /**
             *  Dummy constructor
             */
            public function __construct()
            {
                parent::__construct(
                    Request::getActual(),
                    Config::__set_state(['configFolder' => __DIR__, 'projectFolder' => __DIR__, 'data' => []])
                );
            }
        };
    }

    /**
     * Tests if getRouters() returns an empty array
     *
     * @return void
     */
    public function testGetRouters(): void
    {
        $this->assertEquals([], $this->getDummyMiddleware()->getRouters());
    }

    /**
     * Tests if getPostProcessors() returns an empty array
     *
     * @return void
     */
    public function testGetPostProcessors(): void
    {
        $this->assertEquals([], $this->getDummyMiddleware()->getPostProcessors());
    }
}
