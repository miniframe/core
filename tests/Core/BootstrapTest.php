<?php

namespace Miniframe\Core;

use PHPUnit\Framework\TestCase;

class BootstrapTest extends TestCase
{
    /**
     * Temporary folder that contains a project, created in setUpBeforeClass and destroyed in tearDownAfterClass
     *
     * @var string
     */
    private static $projectFolder;
    /**
     * Path to the test ini files, which are created in setUpBeforeClass and destroyed in tearDownAfterClass
     *
     * @var string
     */
    private static $iniFolder;

    /**
     * This method is called before the first test of this test class is run and prepares a temporary config folder.
     *
     * @return void
     */
    public static function setUpBeforeClass(): void
    {
        parent::setUpBeforeClass();
        $projectFolder = tempnam(sys_get_temp_dir(), 'unittest');
        if ($projectFolder === false) {
            throw new \RuntimeException('Could not create temporary project files');
        }
        self::$projectFolder = $projectFolder;
        if (file_exists(self::$projectFolder)) {
            unlink(self::$projectFolder);
        }
        self::$iniFolder = self::$projectFolder . '/config';
        mkdir(self::$iniFolder, 0777, true);
        file_put_contents(self::$iniFolder . '/main.ini', '[framework]' . PHP_EOL);
        if (!file_exists(self::$iniFolder . '/main.ini')) {
            throw new \RuntimeException('Could not create temporary ini files');
        }
    }

    /**
     * This method is called after the last test of this test class is run and cleans up the temporary config folder.
     *
     * @return void
     */
    public static function tearDownAfterClass(): void
    {
        parent::tearDownAfterClass();
        unlink(self::$iniFolder . '/main.ini');
        rmdir(self::$iniFolder);
        rmdir(self::$projectFolder);
    }

    /**
     * When no routers are configured, a 500 error is the result
     *
     * @return void
     */
    public function testNoRouters(): void
    {
        $result = $this->runBootstrap('[framework]');
        $this->assertEquals(1, $result['exitCode']);
        $this->assertEquals(500, $result['responseCode']);
        $this->assertStringContains('No routers found', $result['output']);
    }

    /**
     * Tests a custom defined Config object
     *
     * @return void
     */
    public function testCustomConfig(): void
    {
        $iniData = '[framework]' . PHP_EOL
            . 'base_href = /' . PHP_EOL
            . 'middleware[] = Miniframe\Middleware\UrlToMvcRouter' . PHP_EOL
            . 'config = App\Core\CustomConfig' . PHP_EOL;

        $result = $this->runBootstrap($iniData, '/index/custom_config');
        $this->assertEquals(0, $result['exitCode']);
        $this->assertEquals(200, $result['responseCode']);
        $this->assertEquals('value', $result['output']);
    }

    /**
     * Tests a custom defined Config object failure on a non-existent class
     *
     * @return void
     */
    public function testCustomConfigNonExistent(): void
    {
        $iniData = '[framework]' . PHP_EOL
            . 'base_href = /' . PHP_EOL
            . 'middleware[] = Miniframe\Middleware\UrlToMvcRouter' . PHP_EOL
            . 'config = App\FooBar\CustomConfig' . PHP_EOL;

        $result = $this->runBootstrap($iniData, '/index/custom_config');
        $this->assertEquals(1, $result['exitCode']);
        $this->assertEquals(500, $result['responseCode']);
        $this->assertStringContains('can\'t be found', $result['output']);
    }

    /**
     * Tests a custom defined Config object failure on a bad class
     *
     * @return void
     */
    public function testCustomConfigWrongInterface(): void
    {
        $iniData = '[framework]' . PHP_EOL
            . 'base_href = /' . PHP_EOL
            . 'middleware[] = Miniframe\Middleware\UrlToMvcRouter' . PHP_EOL
            . 'config = App\Core\CustomNotConfig' . PHP_EOL;

        $result = $this->runBootstrap($iniData, '/index/custom_config');
        $this->assertEquals(1, $result['exitCode']);
        $this->assertEquals(500, $result['responseCode']);
        $this->assertStringContains('isn\'t a valid config class', $result['output']);
    }

    /**
     * When a middleware can't be found, a 500 error is the result
     *
     * @return void
     */
    public function testMiddlewareNotFound(): void
    {
        $illegalMiddleware = 'Miniframe\\Middleware\\ThisDoesNotExistAtAll';

        $iniData = '[framework]' . PHP_EOL
            . 'base_href = /' . PHP_EOL
            . 'middleware[] = ' . $illegalMiddleware . PHP_EOL
        ;
        $result = $this->runBootstrap($iniData);
        $this->assertEquals(1, $result['exitCode']);
        $this->assertEquals(500, $result['responseCode']);
        $this->assertStringContains('Middleware ' . $illegalMiddleware . ' not found', $result['output']);
    }

    /**
     * When a class isn't a valid middleware, a 500 error is the result
     *
     * @return void
     */
    public function testInvalidMiddleware(): void
    {
        $iniData = '[framework]' . PHP_EOL
            . 'base_href = /' . PHP_EOL
            . 'middleware[] = Miniframe\Middleware\InvalidMiddleware' . PHP_EOL
        ;
        $result = $this->runBootstrap($iniData);
        $this->assertEquals(1, $result['exitCode']);
        $this->assertEquals(500, $result['responseCode']);
        $this->assertStringContains('does not extend ' . AbstractMiddleware::class, $result['output']);
    }

    /**
     * When a config class isn't a valid config object, a 500 error is the result
     *
     * @return void
     */
    public function testInvalidConfigValue(): void
    {
        $iniData = '[framework]' . PHP_EOL
            . 'base_href = /' . PHP_EOL
            . 'config[] = Miniframe\Middleware\InvalidMiddleware' . PHP_EOL
        ;
        $result = $this->runBootstrap($iniData);
        $this->assertEquals(1, $result['exitCode']);
        $this->assertEquals(500, $result['responseCode']);
        $this->assertStringContains('Invalid config value', $result['output']);
    }

    /**
     * Requests the root page
     *
     * @return void
     */
    public function testSuccessRun(): void
    {
        $iniData = '[framework]' . PHP_EOL
            . 'base_href = /' . PHP_EOL
            . 'middleware[] = Miniframe\Middleware\UrlToMvcRouter' . PHP_EOL
        ;
        $result = $this->runBootstrap($iniData);
        $this->assertEquals(0, $result['exitCode']);
        $this->assertEquals(200, $result['responseCode']);
        $this->assertStringContains('success', $result['output']);
    }

    /**
     * Requests the root page
     *
     * @return void
     */
    public function testNotFoundRun(): void
    {
        $iniData = '[framework]' . PHP_EOL
            . 'base_href = /' . PHP_EOL
            . 'middleware[] = Miniframe\Middleware\UrlToMvcRouter' . PHP_EOL
        ;
        $result = $this->runBootstrap($iniData, '/404notfound');
        $this->assertEquals(1, $result['exitCode']);
        $this->assertEquals(404, $result['responseCode']);
        $this->assertStringContains('not found', $result['output']);
    }

    /**
     * Requests a redirect
     *
     * @return void
     */
    public function testHeaders(): void
    {
        $iniData = '[framework]' . PHP_EOL
            . 'base_href = /' . PHP_EOL
            . 'middleware[] = Miniframe\Middleware\UrlToMvcRouter' . PHP_EOL
        ;
        $result = $this->runBootstrap($iniData, '/index/redirect');
        $this->assertEquals(0, $result['exitCode']);
        $this->assertEquals(302, $result['responseCode']);
        $this->assertEquals(['Location: https://www.stefanthoolen.nl/'], $result['headers']);
    }

    /**
     * Tests with a working post processor
     *
     * @return void
     */
    public function testValidPostprocessor(): void
    {
        $iniData = '[framework]' . PHP_EOL
            . 'base_href = /' . PHP_EOL
            . 'middleware[] = Miniframe\Middleware\UrlToMvcRouter' . PHP_EOL
            . 'middleware[] = Miniframe\Middleware\PostProcessorTest' . PHP_EOL
        ;
        $result = $this->runBootstrap($iniData);
        $this->assertEquals(0, $result['exitCode']);
        $this->assertEquals(200, $result['responseCode']);
        $this->assertStringContains('success', $result['output']);
    }

    /**
     * Tests with a working post processor
     *
     * @return void
     */
    public function testInalidPostprocessorResponse(): void
    {
        $iniData = '[framework]' . PHP_EOL
            . 'base_href = /' . PHP_EOL
            . 'middleware[] = Miniframe\Middleware\UrlToMvcRouter' . PHP_EOL
            . 'middleware[] = Miniframe\Middleware\PostProcessorTest' . PHP_EOL
        ;
        $result = $this->runBootstrap($iniData, '/invalidResponse');
        $this->assertEquals(1, $result['exitCode']);
        $this->assertEquals(500, $result['responseCode']);
        $this->assertStringContains('doesn\'t return a Response object', $result['output']);
    }

    /**
     * Requests a bad response
     *
     * @return void
     */
    public function testBadResponse(): void
    {
        $iniData = '[framework]' . PHP_EOL
            . 'base_href = /' . PHP_EOL
            . 'middleware[] = Miniframe\Middleware\UrlToMvcRouter' . PHP_EOL
        ;
        $result = $this->runBootstrap($iniData, '/index/badResponse');
        $this->assertEquals(1, $result['exitCode']);
        $this->assertEquals(500, $result['responseCode']);
        $this->assertStringContains('Bad response type', $result['output']);
    }

    /**
     * Runs the bootstrapper, with specific parameters
     *
     * @param string $iniData    Contents of the .ini file.
     * @param string $requestUri The requested URI.
     *
     * @return array{output: string, exitCode: int, responseCode: int, headers: string[]}
     */
    private function runBootstrap(string $iniData, string $requestUri = '/'): array
    {
        // Write down the configuration
        file_put_contents(self::$iniFolder . '/main.ini', $iniData);

        // Simulate the request
        $_SERVER['REQUEST_URI'] = $requestUri;

        // Run the bootstrapper
        clear_headers_list();
        ob_start();
        $bootstrap = new Bootstrap();
        $exitCode = $bootstrap->run(self::$projectFolder);

        // Return the result
        return [
            'output'       => ob_get_clean() ?: '',
            'exitCode'     => $exitCode,
            'responseCode' => (int)http_response_code(),
            'headers'      => headers_list(),
        ];
    }

    /**
     * Asserts that string contains a substring
     *
     * @param string $needle   The string that must be in $haystack.
     * @param string $haystack The full string.
     * @param string $message  Optionally a message.
     *
     * @return void
     */
    private function assertStringContains(string $needle, string $haystack, string $message = ''): void
    {
        $this->assertNotFalse(strpos($haystack, $needle), $message);
    }
}

/**
 * Simulates the php header() function
 *
 * @param string       $header        Sets a header.
 * @param boolean      $replace       Replace current header.
 * @param integer|null $response_code HTTP response code (throws exception, use http_response_code() instead!).
 *
 * @return void
 */
function header(string $header, bool $replace = true, int $response_code = null): void
{
    global $headerList;
    if (!isset($headerList)) {
        clear_headers_list();
    }
    if ($response_code !== null) {
        throw new \RuntimeException(
            'Please don\'t set the response code with header(). Use http_response_code() instead.'
        );
    }

    // Replace
    if ($replace && strpos($header, ':') !== false) {
        $key = strtolower(substr($header, 0, strpos($header, ':')));
        foreach ($headerList as &$headerRow) {
            if (strtolower(substr($headerRow, 0, strlen($key))) == $key) {
                $headerRow = $header;
                return;
            }
        }
    }

    // Append
    array_push($headerList, $header);
}

/**
 * Simulates the php header_list() function
 *
 * @return string[]
 */
function headers_list(): array
{
    global $headerList;
    if (!isset($headerList)) {
        clear_headers_list();
    }
    return $headerList;
}

/**
 * Clears the current list of headers
 *
 * @return void
 */
function clear_headers_list(): void
{
    global $headerList;
    $headerList = array();
}
