<?php

namespace Miniframe\Core;

use PHPUnit\Framework\TestCase;

class RegistryTest extends TestCase
{
    /**
     * Test what happens when we ask the registry if it has an unknown object
     *
     * @return void
     */
    public function testRegistryHasUnknown(): void
    {
        $this->assertFalse(Registry::has('unknown'));
    }

    /**
     * Test what happens when we request an unknown object from the registry
     *
     * @return void
     */
    public function testRegistryGetUnknown(): void
    {
        $this->expectException(\RuntimeException::class);
        Registry::get('unknown');
    }

    /**
     * Test Registry::register and Registry::get
     *
     * @return void
     */
    public function testRegistryRegisterAndGet(): void
    {
        Registry::register('UnitTest', $this);
        $this->assertEquals($this, Registry::get('UnitTest'));
    }
}
