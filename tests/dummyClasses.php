<?php

/**
 * Below are a few controllers written for testing
 */

namespace App\Controller;

use Miniframe\Core\AbstractController;
use Miniframe\Core\Response;
use Miniframe\Response\RedirectResponse;

class Index extends AbstractController
{
    /**
     * /index/main
     *
     * @return Response
     */
    public function main(): Response
    {
        return new Response('success');
    }

    /**
     * /index/redirect
     *
     * @return Response
     */
    public function redirect(): Response
    {
        return new RedirectResponse('https://www.stefanthoolen.nl/');
    }

    /**
     * /index/custom_config
     *
     * @return Response
     */
    public function customConfig(): Response
    {
        // @phpstan-ignore-next-line
        return new Response($this->config->get('custom', 'config'));
    }

    /**
     * /index/badResponse
     *
     * @return boolean
     */
    public function badResponse(): bool
    {
        return false;
    }
}

class Foo extends AbstractController
{
    /**
     * /foo/bar
     *
     * @return Response
     */
    public function bar(): Response
    {
        return new Response('baz');
    }
}

class FooBar extends AbstractController
{
    /**
     * /foo_bar/bar_baz
     * /fooBar/barBaz
     *
     * @return Response
     */
    public function barBaz(): Response
    {
        return new Response('barBaz');
    }
}

class Bar /* extends AbstractController */
{
    /**
     * /bar/main
     *
     * @return Response
     */
    public function main(): Response
    {
        return new Response('failure');
    }
}

/**
 * Below is a custom Config object written for testing
 */

namespace App\Core;

use Miniframe\Core\Config;

class CustomConfig extends Config
{
    /**
     * Overwritten the get() method for a fixed value
     *
     * @param string $section Configuration section.
     * @param string $key     Configuration key.
     *
     * @return mixed
     */
    public function get(string $section, string $key)
    {
        if ($section == 'custom' && $key == 'config') {
            return 'value';
        }
        return parent::get($section, $key);
    }
}

class CustomNotConfig
{
}

/**
 * Below are a few middlewares written for testing
 */

namespace Miniframe\Middleware;

use Miniframe\Core\AbstractMiddleware;
use Miniframe\Core\Response;

class InvalidMiddleware /* extends AbstractMiddleware */
{
}

class PostProcessorTest extends AbstractMiddleware
{
    /**
     * Returns a test callable
     *
     * @return callable[]
     */
    public function getPostProcessors(): array
    {
        return [[$this, 'executePostProcessor']];
    }

    /**
     * Tests the post processor
     *
     * @param Response $response The input response.
     *
     * @return Response|null The output response. Hint; actually must never be null
     */
    public function executePostProcessor(Response $response): ?Response
    {
        if ($this->request->getPath(0) == 'invalidResponse') {
            return null;
        }
        return $response;
    }
}
