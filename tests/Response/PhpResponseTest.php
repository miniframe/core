<?php

namespace Miniframe\Response;

use PHPUnit\Framework\TestCase;

class PhpResponseTest extends TestCase
{
    /**
     * Tests the constructor and render methods
     *
     * @return void
     */
    public function testDefineAndRender(): void
    {
        $response = new PhpResponse(__DIR__ . '/../dummyTemplate.php', [
            'foo' => 'bar'
        ]);

        $expectedHtml = 'This is a test bar';
        $this->assertEquals($expectedHtml, $response->render());
    }
}
