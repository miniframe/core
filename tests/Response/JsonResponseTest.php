<?php

namespace Miniframe\Response;

use PHPUnit\Framework\TestCase;

class JsonResponseTest extends TestCase
{
    /**
     * Returns a list of variables that should be rendered properly
     *
     * @return mixed[]
     */
    public function defineAndRenderDataProvider(): array
    {
        return array(
            [null],
            ['foo' => 'bar', 'baz' => false],
        );
    }

    /**
     * Test if the Content-type header is set correctly
     *
     * @return void
     */
    public function testGetHeaders(): void
    {
        $headers = [['header' => 'Content-type: application/json', 'replace' => true]];
        $response = new JsonResponse(['foo' => 'bar']);
        $this->assertEquals($headers, $response->getHeaders());
    }

    /**
     * Tests the constructor and render methods
     *
     * @param mixed $data The data.
     *
     * @dataProvider defineAndRenderDataProvider
     *
     * @return void
     */
    public function testDefineAndRender($data): void
    {
        $response = new JsonResponse($data);
        $json = $response->render();

        $this->assertJsonStringEqualsJsonString(json_encode($data, JSON_THROW_ON_ERROR), $json);
    }
}
