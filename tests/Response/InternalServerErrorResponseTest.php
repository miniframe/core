<?php

namespace Miniframe\Response;

use PHPUnit\Framework\TestCase;

class InternalServerErrorResponseTest extends TestCase
{
    /**
     * Returns a few parameters to be tested for several methods
     *
     * @return array<string, array{string, int, \Throwable|null}>
     */
    public function responseDataProvider(): array
    {
        return array(
            'With previous exception' =>    ['',             0,   new \RuntimeException()],
            'Without previous exception' => ['',             0,   null],
            'With message' =>               ['With message', 0,   null],
            'With code' =>                  ['With message', 100, null],
        );
    }

    /**
     * Tests if the response code is set correctly to 500
     *
     * @param string          $message  The Exception message to throw.
     * @param integer         $code     The Exception code.
     * @param \Throwable|null $previous The previous throwable used for the exception chaining.
     *
     * @dataProvider responseDataProvider
     *
     * @return void
     */
    public function testGetResponseCode(string $message, int $code, ?\Throwable $previous): void
    {
        $response = new InternalServerErrorResponse($message, $code, $previous);
        $this->assertEquals(500, $response->getResponseCode());
    }

    /**
     * Tests if the exit code is set correctly to 1
     *
     * @param string          $message  The Exception message to throw.
     * @param integer         $code     The Exception code.
     * @param \Throwable|null $previous The previous throwable used for the exception chaining.
     *
     * @dataProvider responseDataProvider
     *
     * @return void
     */
    public function testGetExitCode(string $message, int $code, ?\Throwable $previous): void
    {
        $response = new InternalServerErrorResponse($message, $code, $previous);
        $this->assertEquals(1, $response->getExitCode());
    }

    /**
     * Tests if we can render a page
     *
     * @param string          $message  The Exception message to throw.
     * @param integer         $code     The Exception code.
     * @param \Throwable|null $previous The previous throwable used for the exception chaining.
     *
     * @dataProvider responseDataProvider
     *
     * @return void
     */
    public function testRender(string $message, int $code, ?\Throwable $previous): void
    {
        $response = new InternalServerErrorResponse($message, $code, $previous);
        $this->assertIsString($response->render());
    }

    /**
     * Tests if we can render a page to json format
     *
     * @param string          $message  The Exception message to throw.
     * @param integer         $code     The Exception code.
     * @param \Throwable|null $previous The previous throwable used for the exception chaining.
     *
     * @dataProvider responseDataProvider
     *
     * @return void
     */
    public function testRenderJson(string $message, int $code, ?\Throwable $previous): void
    {
        $_SERVER['HTTP_ACCEPT'] = 'application/json';
        $_SERVER['SERVER_SIGNATURE'] = 'SERVER_SIGNATURE';
        $result = (new InternalServerErrorResponse($message, $code, $previous))->render();
        unset($_SERVER['SERVER_SIGNATURE']);
        unset($_SERVER['HTTP_ACCEPT']);
        $this->assertJson($result);
    }
}
