<?php

namespace Miniframe\Response;

use PHPUnit\Framework\TestCase;

class StreamResponseTest extends TestCase
{
    /**
     * Tests the constructor and render methods
     *
     * @return void
     */
    public function testMemoryResource(): void
    {
        $contents = 'Test 123';

        // Create resource in memory and puts pointer at the beginning
        $fp = fopen('php://memory', 'r+');
        $this->assertIsResource($fp);
        fputs($fp, $contents);
        rewind($fp);

        // Creates a stream response
        $response = new StreamResponse($fp, 'test.txt', 'text/plain', StreamResponse::DISPOSITION_INLINE);

        // Test headers
        $headers = $response->getHeaders();
        $this->assertContains(['header' => 'Content-type: text/plain', 'replace' => true], $headers);
        $this->assertContains(['header' => 'Content-length: ' . strlen($contents), 'replace' => true], $headers);
        $this->assertContains(['header' => 'Content-Transfer-Encoding: Binary', 'replace' => true], $headers);
        $this->assertContains(
            ['header' => 'Content-disposition: inline; filename="test.txt"', 'replace' => true],
            $headers
        );

        // Test output through response value and output buffer
        ob_start();
        $this->assertEquals('', $response->render()); // render() should return empty string
        $data = ob_get_clean();
        $this->assertEquals($contents, $data); // Output buffer should contain the contents
    }

    /**
     * Tests the constructor and render methods
     *
     * @return void
     */
    public function testFileResource(): void
    {
        // Open current file for testing
        $fp = fopen(__FILE__, 'r');
        $this->assertIsResource($fp);

        // Creates a stream response
        $response = new StreamResponse($fp, 'test.php', 'application/php');

        // Test headers
        $headers = $response->getHeaders();
        $this->assertContains(['header' => 'Content-type: application/php', 'replace' => true], $headers);
        $this->assertContains(['header' => 'Content-Transfer-Encoding: Binary', 'replace' => true], $headers);
        $this->assertContains(
            ['header' => 'Content-disposition: attachment; filename="test.php"', 'replace' => true],
            $headers
        );

        // Test output through response value and output buffer
        ob_start();
        $this->assertEquals('', $response->render()); // render() should return empty string
        $data = ob_get_clean();
    }

    /**
     * Tests an invalid resource
     *
     * @return void
     */
    public function testInvalidResource(): void
    {
        $this->expectException(\InvalidArgumentException::class);
        // @phpstan-ignore-next-line
        new StreamResponse('test', 'test.php', 'application/php');
    }

    /**
     * Tests an invalid disposition
     *
     * @return void
     */
    public function testInvalidDisposition(): void
    {
        // Open current file for testing
        $fp = fopen(__FILE__, 'r');
        $this->assertIsResource($fp);

        $this->expectException(\InvalidArgumentException::class);
        new StreamResponse($fp, 'test.php', 'application/php', 3);
    }
}
