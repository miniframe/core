<?php

namespace Miniframe\Response;

use PHPUnit\Framework\TestCase;

class UnauthorizedResponseTest extends TestCase
{
    /**
     * Tests if the response code is set correctly to 401
     *
     * @return void
     */
    public function testGetResponseCode(): void
    {
        $response = new UnauthorizedResponse('My secret place');
        $this->assertEquals(401, $response->getResponseCode());
    }

    /**
     * Tests if the exit code is set correctly to 1
     *
     * @return void
     */
    public function testGetExitCode(): void
    {
        $response = new UnauthorizedResponse('My secret place');
        $this->assertEquals(1, $response->getExitCode());
    }

    /**
     * Tests if we can render a page
     *
     * @return void
     */
    public function testRender(): void
    {
        $response = new UnauthorizedResponse('My secret place');
        $this->assertIsString($response->render());
    }

    /**
     * Test if the WWW-Authenticate header is set correctly
     *
     * @return void
     */
    public function testGetHeaders(): void
    {
        $realm = 'My secret place';

        $headers = [['header' =>
            'WWW-Authenticate: Basic realm="' . str_replace('"', '\\"', $realm) . '"'
            , 'replace' => true]];
        $response = new UnauthorizedResponse($realm);
        $this->assertEquals($headers, $response->getHeaders());
    }

    /**
     * Tests if we can render a page to json format
     *
     * @return void
     */
    public function testRenderJson(): void
    {
        $_SERVER['HTTP_ACCEPT'] = 'application/json';
        $_SERVER['SERVER_SIGNATURE'] = 'SERVER_SIGNATURE';
        $result = (new UnauthorizedResponse('My secret place'))->render();
        unset($_SERVER['SERVER_SIGNATURE']);
        unset($_SERVER['HTTP_ACCEPT']);
        $this->assertJson($result);
    }
}
