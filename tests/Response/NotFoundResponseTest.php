<?php

namespace Miniframe\Response;

use PHPUnit\Framework\TestCase;

class NotFoundResponseTest extends TestCase
{
    /**
     * Tests if the response code is set correctly to 404
     *
     * @return void
     */
    public function testGetResponseCode(): void
    {
        $response = new NotFoundResponse();
        $this->assertEquals(404, $response->getResponseCode());
    }

    /**
     * Tests if the exit code is set correctly to 1
     *
     * @return void
     */
    public function testGetExitCode(): void
    {
        $response = new NotFoundResponse();
        $this->assertEquals(1, $response->getExitCode());
    }

    /**
     * Tests if we can render a page
     *
     * @return void
     */
    public function testRender(): void
    {
        $response = new NotFoundResponse();
        $this->assertIsString($response->render());
    }

    /**
     * Tests if we can render a page to json format
     *
     * @return void
     */
    public function testRenderJson(): void
    {
        $_SERVER['HTTP_ACCEPT'] = 'application/json';
        $_SERVER['SERVER_SIGNATURE'] = 'SERVER_SIGNATURE';
        $result = (new NotFoundResponse())->render();
        unset($_SERVER['SERVER_SIGNATURE']);
        unset($_SERVER['HTTP_ACCEPT']);
        $this->assertJson($result);
    }
}
