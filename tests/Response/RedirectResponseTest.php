<?php

namespace Miniframe\Response;

use PHPUnit\Framework\TestCase;

class RedirectResponseTest extends TestCase
{
    /**
     * Returns a few parameters to be tested for several methods
     *
     * @return array{url: string, int}[]
     */
    public function responseDataProvider(): array
    {
        return array(
            ['url' => 'https://www.stefanthoolen.nl/', RedirectResponse::MOVED_PERMANENTLY],
            ['url' => 'https://www.stefanthoolen.nl/', RedirectResponse::FOUND],
            ['url' => 'https://www.stefanthoolen.nl/', RedirectResponse::SEE_OTHER],
            ['url' => 'https://www.stefanthoolen.nl/', RedirectResponse::TEMPORARY_REDIRECT],
            ['url' => 'https://www.stefanthoolen.nl/', RedirectResponse::PERMANENT_REDIRECT],
        );
    }

    /**
     * Tests if the response code is set correctly to 404
     *
     * @param string  $url          URL to redirect to.
     * @param integer $responseCode One of the HTTP response codes for a redirect.
     *
     * @dataProvider responseDataProvider
     *
     * @return void
     */
    public function testGetResponseCode(string $url, int $responseCode): void
    {
        $response = new RedirectResponse($url, $responseCode);
        $this->assertEquals($responseCode, $response->getResponseCode());
    }

    /**
     * Tests if we can render a page
     *
     * @param string  $url          URL to redirect to.
     * @param integer $responseCode One of the HTTP response codes for a redirect.
     *
     * @dataProvider responseDataProvider
     *
     * @return void
     */
    public function testRender(string $url, int $responseCode): void
    {
        $response = new RedirectResponse($url, $responseCode);
        $this->assertIsString($response->render());
    }

    /**
     * Test if the Content-type header is set correctly
     *
     * @param string  $url          URL to redirect to.
     * @param integer $responseCode One of the HTTP response codes for a redirect.
     *
     * @dataProvider responseDataProvider
     *
     * @return void
     */
    public function testGetHeaders(string $url, int $responseCode): void
    {
        $headers = [['header' => 'Location: ' . $url, 'replace' => true]];
        $response = new RedirectResponse($url, $responseCode);
        $this->assertEquals($headers, $response->getHeaders());
    }

    /**
     * Tests with an invalid response code (at least for redirects)
     *
     * @return void
     */
    public function testInvalidResponseCode(): void
    {
        $this->expectException(\RuntimeException::class);
        new RedirectResponse('https://www.stefanthoolen.nl/', 200);
    }
}
