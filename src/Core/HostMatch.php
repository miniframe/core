<?php

namespace Miniframe\Core;

class HostMatch
{
    /**
     * Check if we're allowed
     *
     * @param Config $config       Reference to the config.
     * @param string $section      The configuration section (accesslist or forwarded-for).
     * @param string $key          The configuration key (proxyhost, allow or deny).
     * @param string $matchAddress IP to validate.
     *
     * @return boolean|null Null when the section doesn't exist.
     */
    public function matchList(Config $config, string $section, string $key, string $matchAddress): ?bool
    {
        if (!$config->has($section, $key)) {
            return null;
        }

        // Fetch validated entries
        $validatedEntries = $config->get($section, $key);
        if (!is_array($validatedEntries)) {
            throw new \RuntimeException(ucfirst($section . ':' . $key) . ' list must be an array');
        }

        // Validate each entry
        foreach ($validatedEntries as $requiredEntry) {
            if ($this->matchEntry($requiredEntry, $matchAddress)) {
                return true;
            }
        }

        return false;
    }

    /**
     * Validates if an entry matches
     *
     * @param string $hostMatch    IP address containing wildcards (as configured).
     * @param string $matchAddress The client IP.
     *
     * @return boolean
     */
    private function matchEntry(string $hostMatch, string $matchAddress): bool
    {
        // Match all
        if (strtolower($hostMatch) == 'all') {
            return true;
        }
        // Exact match, that's easy
        if ($hostMatch === $matchAddress) {
            return true;
        }

        // Match IPv4 or IPv6 address, or a full hostname match
        if (
            preg_match('/([0-9]{1,3}|\*)\.([0-9]{1,3}|\*)/', $hostMatch) // Could include wildcards and CIDR header
            && filter_var($matchAddress, FILTER_VALIDATE_IP, FILTER_FLAG_IPV4)
        ) {
            return $this->matchIpv4($hostMatch, $matchAddress);
        } elseif (
            preg_match('/([0-9a-f]{1,5}\:|\:[0-9a-f]{1,5})/i', $hostMatch) // Could include CIDR header
            && filter_var($matchAddress, FILTER_VALIDATE_IP, FILTER_FLAG_IPV6)
        ) {
            return $this->matchIpv6($hostMatch, $matchAddress);
        } elseif (
            // At least one letter and one dot, so it's no IPv4 (numeric only) and no IPv6 (no dots), but no wildcards
            strpos($hostMatch, '.') !== false
            && strpos($hostMatch, '*') === false
            && preg_match('/[a-z]+/i', $hostMatch)
        ) {
            return $this->matchExactHostname($hostMatch, $matchAddress);
        }

        throw new \RuntimeException('Invalid IP address format: ' . $hostMatch);
    }

    /**
     * Resolves a hostname and matches against the IP address(es) we get back
     *
     * @param string $hostname     The hostname that should be resolved.
     * @param string $matchAddress Client IP address to match to.
     *
     * @return boolean
     */
    private function matchExactHostname(string $hostname, string $matchAddress): bool
    {
        if (filter_var($matchAddress, FILTER_VALIDATE_IP, FILTER_FLAG_IPV4)) {
            $dnsRecords = dns_get_record($hostname, DNS_A);
            if (is_array($dnsRecords)) {
                foreach ($dnsRecords as $dnsRecord) {
                    if ($dnsRecord['type'] === 'A' && $dnsRecord['ip'] == $matchAddress) {
                        return true;
                    }
                }
            }
        }
        if (filter_var($matchAddress, FILTER_VALIDATE_IP, FILTER_FLAG_IPV6)) {
            $dnsRecords = dns_get_record($hostname, DNS_AAAA);
            if (is_array($dnsRecords)) {
                foreach ($dnsRecords as $dnsRecord) {
                    if (
                        $dnsRecord['type'] === 'AAAA'
                        && $this->ipv6ToBits($dnsRecord['ipv6']) == $this->ipv6ToBits($matchAddress)
                    ) {
                        return true;
                    }
                }
            }
        }

        return false;
    }

    /**
     * Validates if an IPv4 address matches
     *
     * @param string $hostMatch    IP address containing wildcards (as configured).
     * @param string $matchAddress The client IP.
     *
     * @return boolean
     */
    private function matchIpv4(string $hostMatch, string $matchAddress): bool
    {
        // When the IP is notated as 192.168.2.*, we'll match
        if (strpos($hostMatch, '*') !== false) {
            $regex = preg_quote($hostMatch, '/');
            $regex = '/^' . str_replace('\\*', '[0-9]{1,3}', $regex) . '$/';
            return preg_match($regex, $matchAddress) > 0;
        }
        // When the IP is notated as 192.168.2.1/24, we'll match
        if (strpos($hostMatch, '/') !== false) {
            list($range, $netmask) = explode('/', $hostMatch, 2);
            $range_decimal = ip2long($range);
            $ip_decimal = ip2long($matchAddress);
            $wildcard_decimal = pow(2, ( 32 - (int)$netmask )) - 1;
            $netmask_decimal = ~ $wildcard_decimal;
            return ( ( $ip_decimal & $netmask_decimal ) == ( $range_decimal & $netmask_decimal ) );
        }

        // No match
        return false;
    }

    /**
     * Validates if an IPv6 address matches
     *
     * @param string $hostMatch    IP address containing wildcards (as configured).
     * @param string $matchAddress The client IP.
     *
     * @return boolean
     */
    private function matchIpv6(string $hostMatch, string $matchAddress): bool
    {
        // Exact match
        if ($this->ipv6ToBits($matchAddress) === $this->ipv6ToBits($hostMatch)) {
            return true;
        }

        // When the IP is notated as 21DA:00D3:0000:2F3B::/64, we'll match
        if (strpos($hostMatch, '/') !== false) {
            list($net, $maskBits) = explode('/', $this->ipv6ToBits($hostMatch));
            $ip_net_bits = substr($this->ipv6ToBits($matchAddress), 0, (int)$maskBits);
            $net_bits = substr($net, 0, (int)$maskBits);
            return ($ip_net_bits == $net_bits);
        }

        return false;
    }

    /**
     * Converts an IPv6 address to bits
     *
     * @param string $ipv6Address The Ipv6 notated address.
     *
     * @return string
     */
    private function ipv6ToBits(string $ipv6Address): string
    {
        $parts = explode('/', $ipv6Address, 2); // CIDR header will remain untouched

        $ip = inet_pton($parts[0]);
        $binary = '';
        foreach (str_split((string)$ip) as $char) {
            $binary .= str_pad(decbin(ord($char)), 8, '0', STR_PAD_LEFT);
        }

        $parts[0] = $binary;

        return implode('/', $parts);
    }
}
