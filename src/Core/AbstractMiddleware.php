<?php

namespace Miniframe\Core;

abstract class AbstractMiddleware
{
    /**
     * Reference to the Request object
     *
     * @var Request
     */
    protected $request;
    /**
     * Reference to the Config object
     *
     * @var Config
     */
    protected $config;

    /**
     * Initializes a Middleware package
     *
     * @param Request $request Reference to the Request object.
     * @param Config  $config  Reference to the Config object.
     */
    public function __construct(Request $request, Config $config)
    {
        $this->request = $request;
        $this->config = $config;
    }

    /**
     * Returns a list of callables that can convert a Config + Request object to a Controller callable.
     *
     * For a good example, see ../Middleware/UrlToMvcRouter.php
     *
     * @return callable[]
     */
    public function getRouters(): array
    {
        return [];
    }

    /**
     * Returns a list of Post Processors that can modify the response after rendering
     *
     * @return callable[]
     */
    public function getPostProcessors(): array
    {
        return [];
    }
}
