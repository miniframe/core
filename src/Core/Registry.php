<?php

namespace Miniframe\Core;

class Registry
{
    /**
     * Collection of all registered objects
     *
     * @var array<string, mixed>
     */
    private static $services = array();

    /**
     * Registers a service
     *
     * @param string $name    Name of the object that needs to be registered.
     * @param mixed  $service Reference to the object.
     *
     * @return void
     */
    public static function register(string $name, $service): void
    {
        self::$services[$name] = $service;
    }

    /**
     * Returns a service
     *
     * @param string $name Name of the registered object.
     *
     * @return mixed
     */
    public static function get(string $name)
    {
        if (!isset(self::$services[$name])) {
            throw new \RuntimeException('Middleware not registered: ' . $name);
        }
        return self::$services[$name];
    }

    /**
     * Validates if a service is registered
     *
     * @param string $name Name of the registered object.
     *
     * @return boolean
     */
    public static function has(string $name): bool
    {
        return isset(self::$services[$name]);
    }
}
