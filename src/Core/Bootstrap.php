<?php

namespace Miniframe\Core;

use Miniframe\Response\InternalServerErrorResponse;
use Miniframe\Response\NotFoundResponse;
use RuntimeException;

/**
 * This is the framework bootstrapper
 */
class Bootstrap
{
    /**
     * Executes the framework
     *
     * @param string $projectFolder Root folder of the project.
     *
     * @return integer
     */
    public function run(string $projectFolder): int
    {
        $projectFolder = rtrim((string)realpath($projectFolder), DIRECTORY_SEPARATOR) . DIRECTORY_SEPARATOR;
        $thrown = false;
        $postProcessors = array();
        try {
            // Load config
            $config = new Config($projectFolder . 'config', $projectFolder);
            if ($config->has('framework', 'config')) {
                $configClassName = $config->get('framework', 'config');
                if (!is_string($configClassName)) {
                    throw new RuntimeException('Invalid config value: ' . var_export($configClassName, true));
                }
                if (!class_exists($configClassName)) {
                    throw new RuntimeException($configClassName . ' can\'t be found');
                }
                if (!in_array(Config::class, class_parents($configClassName))) {
                    throw new RuntimeException($configClassName . ' isn\'t a valid config class');
                }
                $config = new $configClassName($projectFolder . 'config', $projectFolder);
            }
            Registry::register('config', $config);

            // Fetch request and loads middlewares
            $request = Request::getActual();
            $routers = array();
            if ($config->has('framework', 'middleware')) { // @phpstan-ignore-line
                foreach ($config->get('framework', 'middleware') as $key => $FQCN) { // @phpstan-ignore-line
                    if (!class_exists($FQCN)) {
                        throw new RuntimeException('Middleware ' . $FQCN . ' not found');
                    }
                    if (!is_subclass_of($FQCN, AbstractMiddleware::class)) {
                        throw new RuntimeException(
                            'Middleware ' . $FQCN . ' does not extend ' . AbstractMiddleware::class
                        );
                    }
                    if (is_numeric($key)) { // When a value is defined as "middleware[] = FQCN", the key is numeric
                        $key = $FQCN;
                    }
                    $middleware = new $FQCN($request, $config); /* @var $middleware AbstractMiddleware */
                    Registry::register($key, $middleware);
                    $routers = array_merge($routers, $middleware->getRouters());
                    $postProcessors = array_merge($postProcessors, $middleware->getPostProcessors());
                }
            }
            if (count($routers) == 0) {
                throw new RuntimeException(
                    'No routers found. Please define a router middleware. See '
                    . 'https://bitbucket.org/miniframe/core/src/master/README.md'
                    . ' for more.'
                );
            }

            // Tries all routers to find if we can serve a page
            $callable = null;
            foreach ($routers as $router) {
                $callable = call_user_func($router);
                if ($callable !== null) {
                    break;
                }
            }
            if ($callable === null) {
                throw new NotFoundResponse();
            }

            // Executes the callable and validates it's response
            $response = call_user_func($callable);
            if (!is_object($response) || !is_a($response, Response::class)) {
                throw new RuntimeException(
                    'Bad response type: ' . (is_object($response) ? get_class($response) : gettype($response))
                );
            }
        } catch (Response $exception) {
            $response = $exception;
            $thrown = true;
        } catch (\Throwable $exception) {
            $response = new InternalServerErrorResponse('', 0, $exception);
            $thrown = true;
        }

        // Post-processing, in a separate try/catch block, just to be sure
        try {
            $postProcessors = array_reverse($postProcessors);
            foreach ($postProcessors as $postProcessor) {
                $response = call_user_func($postProcessor, $response, $thrown);
                if (!is_a($response, Response::class)) {
                    throw new RuntimeException('One of the PostProcessors doesn\'t return a Response object');
                }
            }
        } catch (\Throwable $exception) {
            $response = new InternalServerErrorResponse('', 0, $exception);
        }

        // Output result
        http_response_code($response->getResponseCode());
        foreach ($response->getHeaders() as $header) {
            header($header['header'], $header['replace']);
        }
        echo $response->render();
        return $response->getExitCode();
    }
}
