<?php

namespace Miniframe\Core;

/**
 * Request object
 *
 * This class represents a HTTP(S) or shell request
 * To get the actual request, use Request::getActual()
 *
 * Shell requests are automatically translated to a path and request data. These requests are the same:
 * - /foo/bar?page=1
 * - php index.php foo bar --page=1
 */
class Request
{
    /**
     * A list of all request ($_GET) data
     *
     * @var array<string, string|string[]>
     */
    private $request_data = array();

    /**
     * A list of all shell arguments (parsed from $_SERVER['argv'])
     *
     * @var array<int|string, mixed>
     */
    private $shell_arguments = array();

    /**
     * A list of all posted ($_POST) data
     *
     * @var array<string, string|string[]>
     */
    private $post_data = array();

    /**
     * Payload of the request
     *
     * @var null|string
     */
    private $payload_data = null;

    /**
     * A list of all posted ($_FILES) files
     *
     * @var  mixed[]
     * @todo Write a get method for posted files
     */
    private $files_data = array();

    /**
     * A list of all server ($_SERVER) data
     *
     * @var array<string, string|string[]>
     */
    private $server_data = array();

    /**
     * Path of the request
     *
     * @var string[]
     */
    private $path = array();

    /**
     * Creates a new request
     *
     * @param array<string, string|string[]> $server_data  A list of all server ($_SERVER) data.
     * @param array<string, string|string[]> $request_data A list of all request ($_GET) data.
     * @param array<string, string|string[]> $post_data    A list of all posted ($_POST) data.
     * @param mixed[]                        $files_data   A list of all posted ($_FILES) files.
     * @param string|null                    $payload_data The full POST payload.
     *
     * @throws \Exception An exception can be thrown when no REQUEST_URI nor argv is populated.
     */
    public function __construct(
        array $server_data = array(),
        array $request_data = array(),
        array $post_data = array(),
        array $files_data = array(),
        string $payload_data = null
    ) {
        $this->server_data = $server_data;
        $this->request_data = $request_data;
        $this->post_data = $post_data;
        $this->files_data = $files_data;
        $this->payload_data = $payload_data;

        // Parses the path
        $this->parsePath();
    }

    /**
     * Returns true when the request is done over HTTPS
     *
     * @return boolean
     */
    public function isHttpsRequest(): bool
    {
        return isset($this->server_data['HTTPS']);
    }

    /**
     * Returns true when the request is done from the shell
     *
     * @return boolean
     */
    public function isShellRequest(): bool
    {
        return php_sapi_name() == 'cli';
    }

    /**
     * Calculates the path and sets it to the 'path' variable.
     *
     * When on a shell, also populates the request_data variable with shell arguments.
     *
     * @return void
     * @throws \Exception An exception can be thrown when no REQUEST_URI nor argv is populated.
     */
    private function parsePath(): void
    {
        // Web request
        if (
            isset($this->server_data['REQUEST_URI'])
            && is_string($this->server_data['REQUEST_URI'])
            && substr($this->server_data['REQUEST_URI'], 0, 1) == '/'
        ) {
            list($request) = explode('?', $this->server_data['REQUEST_URI'], 2);
            if (trim($request, '/') === '') {
                $this->path = array();
                return;
            } else {
                $this->path = explode('/', trim($request, '/'));
                return;
            }
        }

        // Parses shell requests
        if (isset($this->server_data['argv']) && is_array($this->server_data['argv'])) {
            $destination = null;
            $argument_values = $this->server_data['argv'];
            array_shift($argument_values); // Remove the call itself
            foreach ($argument_values as $argument_value) {
                if (substr($argument_value, 0, 2) === '--') {
                    // A parameter with two minus signs, remove one so we catch -- and - in the same code
                    $argument_value = substr($argument_value, 1);
                }
                if (substr($argument_value, 0, 1) === '-') {
                    // A parameter has been specified
                    $destination = substr($argument_value, 1);
                    if (strpos($destination, '=') !== false) {
                        list($destination, $argument_value) = explode("=", $destination, 2);
                    } else {
                        $argument_value = null;
                    }
                }

                if ($destination === null) {
                    // Before any parameter will be considered as path
                    array_push($this->path, $argument_value);
                } elseif (!isset($this->shell_arguments[$destination])) {
                    // First time a parameter is mentioned, make sure it's set
                    $this->shell_arguments[$destination] = $argument_value ? $argument_value : true;
                } elseif ($argument_value === null) {
                    // Parameter is called again, but no new data yet
                    continue;
                } elseif ($this->shell_arguments[$destination] === true) {
                    // First time text is added for a parameter
                    $this->shell_arguments[$destination] = $argument_value;
                } elseif (!is_array($this->shell_arguments[$destination])) {
                    // Parameter is mentioned for the second time, convert to array
                    $this->shell_arguments[$destination] = array(
                        $this->shell_arguments[$destination],
                        $argument_value
                    );
                } else {
                    // Parameter is mentioned even more, just keep on appending data
                    array_push($this->shell_arguments[$destination], $argument_value);
                }
            }
            return;
        }

        throw new \RuntimeException('Validate if $_SERVER["REQUEST_URI"] or $_SERVER["argv"] is set properly');
    }

    /**
     * Returns the path of the web request as array when no index is specified,
     * or a specific path item when an index is specified.
     *
     * Returns null when the index is specified but there are not that many path items.
     * Returns a string when the index is specified and there are enough path items.
     * Returns an array if no index is specified.
     *
     * @param integer|null $index The index of the required path.
     *
     * @return ($index is null ? string[] : string|null)
     */
    public function getPath(int $index = null)
    {
        if ($index === null) {
            return $this->path;
        }

        // When the index is negative one, and we're in a shell, return the called command
        if ($index === -1 && $this->isShellRequest()) {
            return $this->server_data['argv'][0] ?? null;
        }

        return $this->path[$index] ?? null;
    }

    /**
     * Returns all request values when no key is specified, or a specific request value when a key is specified.
     *
     * Returns null when the key is specified but no value is found
     * Returns a string when the key is specified and one value is found
     * Returns an array if no key is specified, or the key has multiple values
     *
     * @param string|null $key The request value to request.
     *
     * @return ($key is null ? array<string, string|string[]> : string|string[]|null)
     */
    public function getRequest(?string $key = null)
    {
        // Defines the request array
        if ($this->isShellRequest()) {
            $var = 'shell_arguments';
        } else {
            $var = 'request_data';
        }

        if ($key === null) {
            return $this->{$var};
        }
        return $this->{$var}[$key] ?? null;
    }

    /**
     * Returns all server values when no key is specified, or a specific server value when a key is specified.
     *
     * Returns null when the key is specified but no value is found
     * Returns a string when the key is specified and one value is found
     * Returns an array if no key is specified, or the key has multiple values
     *
     * @param string|null $key The request value to request.
     *
     * @return ($key is null ? array<string, string|string[]> : string|string[]|null)
     */
    public function getServer(?string $key = null)
    {
        if ($key === null) {
            return $this->server_data;
        }

        return $this->server_data[$key] ?? null;
    }

    /**
     * Returns all posted values when no key is specified, or a specific posted value when a key is specified.
     *
     * Returns null when the key is specified but no value is found
     * Returns a string when the key is specified and one value is found
     * Returns an array if no key is specified, or the key has multiple values
     *
     * @param string|null $key The request value to request.
     *
     * @return ($key is null ? array<string, string|string[]> : string|string[]|null)
     */
    public function getPost(?string $key = null)
    {
        if (isset($key)) {
            return isset($this->post_data[$key]) ? $this->post_data[$key] : null;
        }
        return $this->post_data;
    }

    /**
     * Returns the posted payload data as string. When no data exists, null will be returned.
     *
     * @return string|null
     */
    public function getPayload(): ?string
    {
        return $this->payload_data;
    }

    /**
     * Returns a Request object based on the actual web request
     *
     * @return Request
     */
    public static function getActual(): self
    {
        return static::__set_state([
            'server_data'  => $_SERVER,
            'request_data' => $_GET,
            'post_data'    => $_POST,
            'files_data'   => $_FILES,
            'payload_data' => file_get_contents('php://input') ?: null,
        ]);
    }

    /**
     * Magic method; sets a request object based on a specific state
     *
     * @param array<string, mixed> $data The actual state.
     *
     * @return Request
     * @throws \Exception An exception can be thrown when no REQUEST_URI nor argv is populated.
     * @see    var_export()
     */
    public static function __set_state(array $data): self
    {
        return new self(
            isset($data['server_data']) && is_array($data['server_data']) ? $data['server_data'] : array(),
            isset($data['request_data']) && is_array($data['request_data']) ? $data['request_data'] : array(),
            isset($data['post_data']) && is_array($data['post_data']) ? $data['post_data'] : array(),
            isset($data['files_data']) && is_array($data['files_data']) ? $data['files_data'] : array(),
            isset($data['payload_data']) && is_string($data['payload_data']) ? $data['payload_data'] : null
        );
    }
}
