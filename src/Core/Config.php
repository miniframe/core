<?php

namespace Miniframe\Core;

use RuntimeException;

class Config
{
    /**
     * All config data
     *
     * @var array<string, array<string, mixed>>
     */
    private $data = array();

    /**
     * Path in which the config files are located (absolute path, ending with directory separator)
     *
     * @var string
     */
    private $configFolder;

    /**
     * Root folder of the project (absolute path, ending with directory separator)
     *
     * @var string
     */
    private $projectFolder;

    /**
     * Initializes config; reads all .ini files sorted alphabetically in a specific folder
     *
     * @param string $configFolder  Folder that contains .ini files.
     * @param string $projectFolder Root folder of the project (required for getPath()).
     */
    public function __construct(string $configFolder, string $projectFolder)
    {
        if (!is_dir($projectFolder)) {
            throw new \RuntimeException('Project folder doesn\'t exist: ' . $projectFolder);
        }
        if (!is_dir($configFolder)) {
            throw new \RuntimeException('Config folder doesn\'t exist: ' . $configFolder);
        }
        $this->projectFolder = rtrim((string)realpath($projectFolder), DIRECTORY_SEPARATOR) . DIRECTORY_SEPARATOR;
        $this->configFolder = rtrim((string)realpath($configFolder), DIRECTORY_SEPARATOR) . DIRECTORY_SEPARATOR;
        $configFiles = glob($this->configFolder . '*.ini');
        if (!is_array($configFiles)) {
            // @codeCoverageIgnoreStart
            // This should not happen since we already checked if the dir exists.
            throw new \RuntimeException('No config files found in folder: ' . $configFolder);
            // @codeCoverageIgnoreEnd
        }
        foreach ($configFiles as $configFile) {
            $parsed = parse_ini_file($configFile, true, INI_SCANNER_TYPED);
            if ($parsed === false) {
                // @codeCoverageIgnoreStart
                // There's no case found so far when this returns false, but for strict typing, this is required.
                throw new \RuntimeException('Can\'t parse config file: ' . $configFile);
                // @codeCoverageIgnoreEnd
            }
            $this->appendArray($parsed);
        }
    }

    /**
     * Appends the array with some more complex logics
     *
     * @param array<string, array<string, mixed>> $array The array data to append.
     *
     * @return void
     */
    private function appendArray(array $array): void
    {
        foreach ($array as $sectionName => $sectionData) {
            if (!isset($this->data[$sectionName])) {
                $this->data[$sectionName] = array();
            }
            // Loop through section data
            foreach ($sectionData as $valueName => $valueData) {
                if (!is_array($valueData)) {
                    $this->data[$sectionName][$valueName] = $valueData;
                    continue;
                }
                // If the value is an array, append that as well
                foreach ($valueData as $valueKey => $valueValue) {
                    if (!isset($this->data[$sectionName][$valueName])) {
                        $this->data[$sectionName][$valueName] = array();
                    }
                    if (!is_array($this->data[$sectionName][$valueName])) {
                        $this->data[$sectionName][$valueName] = array($this->data[$sectionName][$valueName]);
                    }
                    if (is_numeric($valueKey)) {
                        $this->data[$sectionName][$valueName][] = $valueValue;
                    } else {
                        $this->data[$sectionName][$valueName][$valueKey] = $valueValue;
                    }
                }
            }
        }
    }

    /**
     * Returns a specific config value
     *
     * @param string $section Configuration section.
     * @param string $key     Configuration key.
     *
     * @return mixed
     */
    public function get(string $section, string $key)
    {
        if (!array_key_exists($section, $this->data)) {
            throw new \RuntimeException("Config section does not exist: " . $section);
        }
        if (!array_key_exists($key, $this->data[$section])) {
            throw new \RuntimeException("Config key (" . $key . ") does not exist in section " . $section);
        }

        return $this->data[$section][$key];
    }

    /**
     * Returns a specific config value and converts it to a path, relative from the project root
     *
     * @param string $section Configuration section.
     * @param string $key     Configuration key.
     *
     * @return mixed
     */
    public function getPath(string $section, string $key)
    {
        $original = $this->get($section, $key);
        if (!is_string($original)) {
            //throw new RuntimeException('Config value ' . $section . '.' . $key . ' is not a string');
        }
        $return = array();
        foreach ((array)$original as $path) {
            if (!is_string($path)) {
                throw new RuntimeException('Invalid path value: ' . var_export($path, true));
            }
            // Absolute path
            if (substr($path, 0, 1) == '/' || substr($path, 1, 1) == ':') {
                $return[] = $path;
                continue;
            }
            // Relative path
            $path = $this->projectFolder . $path;
            // When the path already exists, resolve all relative parts making it absolute
            if (file_exists($path)) {
                $path = (string)realpath($path);
            }
            // When the path is a dir, suffix with a directory separator
            if (is_dir($path)) {
                $path .= DIRECTORY_SEPARATOR;
            }
            $return[] = $path;
        }
        return is_array($original) ? $return : array_shift($return);
    }

    /**
     * Validates if a config value exists
     *
     * @param string $section Configuration section.
     * @param string $key     Configuration key.
     *
     * @return boolean
     */
    public function has(string $section, string $key): bool
    {
        // Using array_key_exists instead of isset, since it's value can be null
        return array_key_exists($section, $this->data) && array_key_exists($key, $this->data[$section]);
    }

    /**
     * Magic method; sets a config object based on a specific state
     *
     * @param array<string, mixed> $data The actual state.
     *
     * @return Config
     * @see    var_export()
     */
    public static function __set_state(array $data): self
    {
        if (
            !isset($data['configFolder']) || !isset($data['projectFolder']) || !isset($data['data'])
            || !is_string($data['configFolder']) || !is_string($data['projectFolder']) || !is_array($data['data'])
        ) {
            throw new \RuntimeException('Required keys not sent (configFolder, projectFolder & data)');
        }

        $return = new self($data['configFolder'], $data['projectFolder']);
        $return->data = $data['data'];

        return $return;
    }
}
