<?php

namespace Miniframe\Core;

class Response extends \Exception
{
    /**
     * Text to return
     *
     * @var string
     */
    private $text;

    /**
     * Exit code to return
     *
     * @var int
     */
    private $exitCode = 0;

    /**
     * HTTP Response code
     *
     * @var int
     */
    private $responseCode = 200;

    /**
     * List of HTTP headers
     *
     * @var array{header: string, replace: bool}[]
     */
    private $headers = array();

    /**
     * Initializes a generic response
     *
     * @param string  $text     Response text.
     * @param integer $exitCode Exit code (used for shell error codes).
     */
    public function __construct(string $text = '', int $exitCode = 0)
    {
        $this->text = $text;
        $this->exitCode = $exitCode;
    }

    /**
     * Returns the response text
     *
     * @return string
     */
    public function render(): string
    {
        return $this->text;
    }

    /**
     * Returns the current exit code (used for shell error codes)
     *
     * @return integer
     */
    public function getExitCode(): int
    {
        return $this->exitCode;
    }

    /**
     * Returns the current response code (used for HTTP responses)
     *
     * @return integer
     */
    public function getResponseCode(): int
    {
        return $this->responseCode;
    }

    /**
     * Sets a new response code (used for HTTP responses)
     *
     * @param integer $responseCode The new response code.
     *
     * @return void
     */
    public function setResponseCode(int $responseCode): void
    {
        $this->responseCode = $responseCode;
    }

    /**
     * Sets a new exit code (used for shell error codes)
     *
     * @param integer $exitCode Error code.
     *
     * @return void
     */
    public function setExitCode(int $exitCode): void
    {
        $this->exitCode = $exitCode;
    }

    /**
     * Adds a response header
     *
     * @param string  $header  The header string.
     * @param boolean $replace When set to false, a header can be sent back multiple times.
     *
     * @return void
     */
    public function addHeader(string $header, bool $replace = true): void
    {
        $this->headers[] = ['header' => $header, 'replace' => $replace];
    }

    /**
     * Returns a list of all headers to set (each record has two keys: 'header' and 'replace')
     *
     * @return array{header: string, replace: bool}[]
     */
    public function getHeaders(): array
    {
        return $this->headers;
    }
}
