<?php

namespace Miniframe\Core;

abstract class AbstractController
{
    /**
     * Reference to the Request object
     *
     * @var Request
     */
    protected $request;
    /**
     * Reference to the Config object
     *
     * @var Config
     */
    protected $config;

    /**
     * Initializes a Controller
     *
     * @param Request $request Reference to the Request object.
     * @param Config  $config  Reference to the Config object.
     */
    public function __construct(Request $request, Config $config)
    {
        $this->request = $request;
        $this->config = $config;
    }
}
