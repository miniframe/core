<?php

namespace Miniframe\Response;

use Miniframe\Core\Request;
use Miniframe\Core\Response;

class UnauthorizedResponse extends Response
{
    /**
     * Construct the response
     *
     * @param string $realm Realm we need to identify to.
     */
    public function __construct(string $realm)
    {
        $request = Request::getActual();
        $signature = Request::getActual()->getServer('SERVER_SIGNATURE');

        // When JSON is requested, return JSON error
        if ($request->getServer('HTTP_ACCEPT') == 'application/json') {
            $data = [
                'code' => 401,
                'error' => 'Unauthorized',
                'message' => $realm,
            ];
            if (is_string($signature)) {
                $data['signature'] = $signature;
            }
            $html = json_encode($data, JSON_PRETTY_PRINT | JSON_THROW_ON_ERROR);
            $this->addHeader('Content-type: application/json');
        } else {
            $html = '<!DOCTYPE HTML PUBLIC "-//IETF//DTD HTML 2.0//EN">' . PHP_EOL
                . '<html><head>' . PHP_EOL
                . '<title>401 Unauthorized</title>' . PHP_EOL
                . '</head><body>' . PHP_EOL
                . '<h1>Unauthorized</h1>' . PHP_EOL
                . '<p>This server could not verify that you' . PHP_EOL
                . 'are authorized to access the document' . PHP_EOL
                . 'requested.  Either you supplied the wrong' . PHP_EOL
                . 'credentials (e.g., bad password), or your' . PHP_EOL
                . 'browser doesn\'t understand how to supply' . PHP_EOL
                . 'the credentials required.</p>' . PHP_EOL
                . '<hr>' . (is_string($signature) ? $signature : '') . PHP_EOL
                . '</body></html>';
        }
        parent::__construct($html, 1);

        $this->setResponseCode(401);
        $this->addHeader('WWW-Authenticate: Basic realm="' . str_replace('"', '\\"', $realm) . '"');
    }
}
