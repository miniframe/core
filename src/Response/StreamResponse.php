<?php

namespace Miniframe\Response;

use Miniframe\Core\Response;

class StreamResponse extends Response
{
    /**
     * Resource of a stream.
     *
     * @var resource
     */
    private $resource;

    /**
     * Disposition options
     */
    public const
        DISPOSITION_ATTACHMENT = 1,
        DISPOSITION_INLINE = 2
    ;

    /**
     * Streams a resource back to the client
     *
     * @param resource    $resource    Resource of a stream (`$resource = fopen('/path/to/large-file.zip', 'r');`).
     * @param string|null $filename    Name of the file.
     * @param string|null $contentType Type of the file.
     * @param integer     $disposition Type of the stream (self::DISPOSITION_ATTACHMENT or self::DISPOSITION_INLINE).
     */
    public function __construct(
        $resource,
        string $filename = null,
        string $contentType = null,
        int $disposition = self::DISPOSITION_ATTACHMENT
    ) {
        if (is_resource($resource) === false) {
            throw new \InvalidArgumentException(
                'Argument must be a valid resource type. ' . gettype($resource) . ' given.'
            );
        }
        $this->resource = $resource;
        switch ($disposition) {
            case self::DISPOSITION_ATTACHMENT:
                $dispositionStr = 'attachment';
                break;
            case self::DISPOSITION_INLINE:
                $dispositionStr = 'inline';
                break;
            default:
                throw new \InvalidArgumentException(
                    'Argument must be a valid disposition. ' . var_export($disposition, true) . ' given.'
                );
        }

        // Add Content Type header, when specified
        if ($contentType !== null) {
            $this->addHeader('Content-type: ' . $contentType);
        }

        // Add Content-length and Last-Modified, when known
        $stat = fstat($resource);
        if (isset($stat['size']) && $stat['size'] > 0) {
            $this->addHeader('Content-length: ' . $stat['size']);
        }
        if (isset($stat['mtime']) && $stat['mtime'] > 0) {
            $this->addHeader('Last-Modified: ' . date('r', $stat['mtime']));
        }

        // Transfer is binary
        $this->addHeader('Content-Transfer-Encoding: Binary');

        // Compile Content-disposition header
        $contentDisposition = 'Content-disposition: ' . $dispositionStr;
        if ($filename !== null) {
            $contentDisposition .= '; filename="' . rawurlencode($filename) . '"';
        }
        $this->addHeader($contentDisposition);
    }

    /**
     * Passes thru the stream data to STDOUT. Always returns an empty string.
     *
     * @return string
     */
    public function render(): string
    {
        $currentLimit = ini_get('max_execution_time');
        set_time_limit(0);
        fpassthru($this->resource);
        set_time_limit((int)$currentLimit);
        fclose($this->resource);
        return '';
    }
}
