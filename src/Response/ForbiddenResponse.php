<?php

namespace Miniframe\Response;

use Miniframe\Core\Request;
use Miniframe\Core\Response;

class ForbiddenResponse extends Response
{
    /**
     * Initializes a basic 403 Forbidden response
     */
    public function __construct()
    {
        parent::__construct('', 1);
        $this->setResponseCode(403);
    }

    /**
     * Returns a basic HTML page explaining that something went wrong.
     *
     * @return string
     */
    public function render(): string
    {
        $request = Request::getActual();
        $signature = $request->getServer('SERVER_SIGNATURE');

        // When JSON is requested, return JSON error
        if ($request->getServer('HTTP_ACCEPT') == 'application/json') {
            $data = [
                'code' => 403,
                'error' => 'Forbidden',
                'message' => 'You don\'t have permission to access this resource.',
            ];
            if ($signature) {
                $data['signature'] = $signature;
            }
            return json_encode($data, JSON_PRETTY_PRINT | JSON_THROW_ON_ERROR);
        }

        return '<!DOCTYPE HTML PUBLIC "-//IETF//DTD HTML 2.0//EN">' . PHP_EOL
            . '<html><head>' . PHP_EOL
            . '<title>403 Forbidden</title>' . PHP_EOL
            . '</head><body>' . PHP_EOL
            . '<h1>Forbidden</h1>' . PHP_EOL
            . '<p>You don\'t have permission to access this resource.</p>' . PHP_EOL
            . '<hr>' . PHP_EOL
            . (is_string($signature) ? $signature : '') . PHP_EOL
            . '</body></html>';
    }
}
