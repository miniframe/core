<?php

namespace Miniframe\Response;

use Miniframe\Core\Request;
use Miniframe\Core\Response;

class RedirectResponse extends Response
{
    /**
     * URL to which we redirect
     *
     * @var string
     */
    private $url;

    public const
        MOVED_PERMANENTLY = 301,  // GET methods unchanged. Others may or may not be changed to GET.
        FOUND = 302,              // GET methods unchanged. Others may or may not be changed to GET.
        SEE_OTHER = 303,          // GET methods unchanged. Others changed to GET (body lost).
        TEMPORARY_REDIRECT = 307, // Method and body not changed.
        PERMANENT_REDIRECT = 308; // Method and body not changed.

    /**
     * Initializes a new Redirect response
     *
     * @param string  $url          URL to redirect to.
     * @param integer $responseCode One of the HTTP response codes for a redirect.
     */
    public function __construct(string $url, int $responseCode = self::FOUND)
    {
        $signature = Request::getActual()->getServer('SERVER_SIGNATURE');
        $html = '<!DOCTYPE HTML PUBLIC "-//IETF//DTD HTML 2.0//EN">' . PHP_EOL
            . '<html><head>' . PHP_EOL
            . '<title>' . $responseCode . ' ' . $this->getResponseText($responseCode) . '</title>' . PHP_EOL
            . '</head><body>' . PHP_EOL
            . '<p>The document has moved <a href="' . htmlspecialchars($url) . '">here</a>.</p>' . PHP_EOL
            . '<hr>' . PHP_EOL
            . (is_string($signature) ? $signature : '') . PHP_EOL
            . '</body></html>';
        parent::__construct($html);

        $this->url = $url;
        $this->setResponseCode($responseCode);
        $this->addHeader('Location: ' . $this->url);
    }

    /**
     * Converts a HTTP status code to it's label
     *
     * @param integer $responseCode One of the HTTP response codes for a redirect.
     *
     * @return string
     */
    private function getResponseText(int $responseCode): string
    {
        if ($responseCode == static::MOVED_PERMANENTLY) {
            return 'Moved permanently';
        }
        if ($responseCode == static::FOUND) {
            return 'Found';
        }
        if ($responseCode == static::SEE_OTHER) {
            return 'See other';
        }
        if ($responseCode == static::TEMPORARY_REDIRECT) {
            return 'Temporary redirect';
        }
        if ($responseCode == static::PERMANENT_REDIRECT) {
            return 'Permanent redirect';
        }
        throw new \RuntimeException('Invalid response code for redirects: ' . $responseCode);
    }
}
