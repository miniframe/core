<?php

namespace Miniframe\Response;

use Miniframe\Core\Request;
use Miniframe\Core\Response;

class NotFoundResponse extends Response
{
    /**
     * Initializes a basic 404 Not Found response
     */
    public function __construct()
    {
        $request = Request::getActual();
        $requestUri = $request->getServer('REQUEST_URI');
        $signature = $request->getServer('SERVER_SIGNATURE');

        $code = 404;
        $error = 'Not Found';
        $message = 'The requested URL '
            . (is_string($requestUri) ? $requestUri : '')
            . ' was not found on this server.';

        if ($request->getServer('HTTP_ACCEPT') == 'application/json') {
            $data = [
                'code' => $code,
                'error' => $error,
                'message' => $message,
            ];
            if (is_string($signature)) {
                $data['signature'] = $signature;
            }
            $text = json_encode($data, JSON_PRETTY_PRINT | JSON_THROW_ON_ERROR);
        } else {
            $text = '<!DOCTYPE HTML PUBLIC "-//IETF//DTD HTML 2.0//EN">' . PHP_EOL
                . '<html><head>' . PHP_EOL
                . '<title>' . htmlspecialchars($code . ' ' . $error) . '</title>' . PHP_EOL
                . '</head><body>' . PHP_EOL
                . '<h1>' . htmlspecialchars($error) . '</h1>' . PHP_EOL
                . '<p>' . htmlspecialchars($message) . '</p>' . PHP_EOL
                . '<hr>' . PHP_EOL
                . (is_string($signature) ? $signature : '') . PHP_EOL
                . '</body></html>';
        }

        parent::__construct($text, 1);
        $this->setResponseCode($code);
    }
}
