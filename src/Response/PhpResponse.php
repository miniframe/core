<?php

namespace Miniframe\Response;

use Miniframe\Core\Response;

class PhpResponse extends Response
{
    /**
     * Path to the PHP file
     *
     * @var string
     */
    private $phpFile;

    /**
     * List of variables available in the scope of the PHP file
     *
     * @var mixed[]
     */
    private $variables;

    /**
     * Uses a PHP file as template.
     *
     * @param string  $phpFile   Path to the PHP file.
     * @param mixed[] $variables List of variables available in the scope of the PHP file.
     */
    public function __construct(string $phpFile, array $variables = array())
    {
        $this->phpFile = $phpFile;
        $this->variables = $variables;
    }

    /**
     * Renders the PHP file and returns the output.
     *
     * @return string
     */
    public function render(): string
    {
        extract($this->variables);
        ob_start();
        require $this->phpFile;
        return ob_get_clean() ?: '';
    }
}
