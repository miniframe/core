<?php

namespace Miniframe\Response;

use Miniframe\Core\Request;
use Miniframe\Core\Response;

class InternalServerErrorResponse extends Response
{
    /**
     * Construct the exception. Note: The message is NOT binary safe.
     *
     * @param string          $message  The Exception message to throw.
     * @param integer         $code     The Exception code.
     * @param \Throwable|null $previous The previous throwable used for the exception chaining.
     *
     * @link https://php.net/manual/en/exception.construct.php
     */
    public function __construct(string $message = '', int $code = 0, \Throwable $previous = null)
    {
        \Exception::__construct($message, $code, $previous);
        $this->setResponseCode(500);
        $this->setExitCode(1);
    }

    /**
     * Returns a basic HTML page explaining that something went wrong.
     *
     * @return string
     */
    public function render(): string
    {
        $request = Request::getActual();
        $signature = $request->getServer('SERVER_SIGNATURE');
        $exceptionMessage = ($this->getCode() || $this->getMessage() ? '<p>Actual exception: ('
                . htmlspecialchars(get_class($this))
                . ') #' . (string)$this->getCode() . ': ' . htmlspecialchars($this->getMessage())
                . '<br>Previous exception:' . PHP_EOL : '<p>Exception:'
            )

            . ($this->getPrevious() ? ' (' . htmlspecialchars(get_class($this->getPrevious())) . ') #'
                . htmlspecialchars($this->getPrevious()->getCode()) . ': '
                . htmlspecialchars($this->getPrevious()->getMessage()) . '</p>' . PHP_EOL : '');

        // When JSON is requested, return JSON error
        if ($request->getServer('HTTP_ACCEPT') == 'application/json') {
            $data = [
                'code' => 500,
                'error' => 'Internal Server Error',
                'message' => strip_tags($exceptionMessage),
            ];
            if ($signature) {
                $data['signature'] = $signature;
            }
            return json_encode($data, JSON_PRETTY_PRINT | JSON_THROW_ON_ERROR);
        }

        return '<!DOCTYPE HTML PUBLIC "-//IETF//DTD HTML 2.0//EN">' . PHP_EOL
            . '<html><head>' . PHP_EOL
            . '<title>500 Internal Server Error</title>' . PHP_EOL
            . '</head><body>' . PHP_EOL
            . '<h1>Internal Server Error</h1>' . PHP_EOL
            . '<p>The server encountered an internal error or misconfiguration and was unable to complete your '
            . 'request.</p><p>Please contact the server administrator <!-- at root@localhost --> to inform them of the '
            . 'time this error occured, and the actions you performed just before this error.</p>'

            . $exceptionMessage

            . '<p>More information about this error may be available in the server log.</p>' . PHP_EOL
            . '<hr>' . PHP_EOL
            . (is_string($signature) ? $signature : '') . PHP_EOL
            . '</body></html>';
    }
}
