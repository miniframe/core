<?php

namespace Miniframe\Response;

use Miniframe\Core\Response;

class JsonResponse extends Response
{
    /**
     * The response data
     *
     * @var mixed
     */
    private $data;

    /**
     * Initializes a Json Response
     *
     * @param mixed $data Response data.
     */
    public function __construct($data)
    {
        $this->data = $data;
        $this->addHeader('Content-type: application/json');
    }

    /**
     * Returns the JSON encoded data collection
     *
     * @return string
     */
    public function render(): string
    {
        return json_encode($this->data, JSON_PRETTY_PRINT | JSON_THROW_ON_ERROR);
    }
}
