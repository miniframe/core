<?php

namespace Miniframe\Middleware;

use Miniframe\Core\AbstractMiddleware;
use Miniframe\Core\Config;
use Miniframe\Core\HostMatch;
use Miniframe\Core\Request;
use ReflectionClass;

class ForwardedFor extends AbstractMiddleware
{
    /**
     * Initializes the ForwardedFor middleware
     *
     * @param Request $request Reference to the Request object.
     * @param Config  $config  Reference to the Config object.
     */
    public function __construct(Request $request, Config $config)
    {
        parent::__construct($request, $config);

        // Do we have an IP address?
        $validateIp = $request->getServer('REMOTE_ADDR');
        if (!is_string($validateIp)) {
            return;
        }

        // Only continue when one of these headers exist
        if (
            $request->getServer('HTTP_X_FORWARDED_FOR') === null
            && $request->getServer('HTTP_X_FORWARDED_HOST') === null
            && $request->getServer('HTTP_X_FORWARDED_PROTO') === null
        ) {
            return;
        }

        // When there's no proxyhost match, skip (null = no directive, true = match proxyhost, false = no match)
        if ((new HostMatch())->matchList($config, 'forwarded-for', 'proxyhost', $validateIp) === false) {
            return;
        }

        // Change Request object
        $reflectionClass = new ReflectionClass(Request::class);
        $reflectionProperty = $reflectionClass->getProperty('server_data');
        $reflectionProperty->setAccessible(true);
        $serverData = $reflectionProperty->getValue($request);
        $this->modifyServerData($serverData);
        $reflectionProperty->setValue($request, $serverData);

        // Change global
        $this->modifyServerData($_SERVER);
    }

    /**
     * Modifies the server data
     *
     * @param array<string, string> $serverData Server data array by reference.
     *
     * @return void
     */
    private function modifyServerData(array &$serverData): void
    {
        // https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/X-Forwarded-For
        if (isset($serverData['HTTP_X_FORWARDED_FOR'])) {
            $serverData['REMOTE_ADDR'] = trim(explode(',', $serverData['HTTP_X_FORWARDED_FOR'], 2)[0]);
            unset($serverData['HTTP_X_FORWARDED_FOR']);
        }

        // https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/X-Forwarded-Host
        if (isset($serverData['HTTP_X_FORWARDED_HOST'])) {
            $serverData['HTTP_HOST'] = $serverData['HTTP_X_FORWARDED_HOST'];
            unset($serverData['HTTP_X_FORWARDED_HOST']);
        }

        // https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/X-Forwarded-Proto
        if (isset($serverData['HTTP_X_FORWARDED_PROTO'])) {
            $currentPort = $serverData['SERVER_PORT'];
            $currentProtocol = isset($serverData['HTTPS']) ? 'https' : 'http';
            if (
                strtolower($serverData['HTTP_X_FORWARDED_PROTO']) == 'https'
                && $currentProtocol == 'http'
            ) {
                $serverData['HTTPS'] = 'on';
                if ($currentPort == 80) {
                    $serverData['SERVER_PORT'] = 443;
                }
            } elseif (
                $serverData['HTTP_X_FORWARDED_PROTO'] == 'http'
                && $currentProtocol == 'https'
            ) {
                unset($serverData['HTTPS']);
                if ($currentPort == 443) {
                    $serverData['SERVER_PORT'] = 80;
                }
            }
            unset($serverData['HTTP_X_FORWARDED_PROTO']);
        }
    }
}
