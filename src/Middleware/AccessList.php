<?php

namespace Miniframe\Middleware;

use Miniframe\Core\AbstractMiddleware;
use Miniframe\Core\Config;
use Miniframe\Core\HostMatch;
use Miniframe\Core\Request;
use Miniframe\Response\ForbiddenResponse;
use RuntimeException;

class AccessList extends AbstractMiddleware
{
    /**
     * Validates a user IP towards an allow/deny list.
     *
     * @param Request $request Reference to the Request object.
     * @param Config  $config  Reference to the Config object.
     */
    public function __construct(Request $request, Config $config)
    {
        parent::__construct($request, $config);

        // Do we have an IP address?
        $validateIp = $request->getServer('REMOTE_ADDR');
        if (!is_string($validateIp)) {
            return;
        }

        // Determine the order
        $order = 'deny,allow';
        if ($this->config->has('accesslist', 'order')) {
            $orderValue = $this->config->get('accesslist', 'order');
            if (!is_string($orderValue)) {
                throw new RuntimeException('Invalid order value: ' . var_export($orderValue, true));
            }
            $order = preg_replace('/[\s]+/', '', strtolower($orderValue));
        }

        if ($order == 'deny,allow') {
            // First, all Deny directives are evaluated; if any match, the request is denied unless it also matches an
            // Allow directive. Any requests which do not match any Allow or Deny directives are permitted.
            if (
                (new HostMatch())->matchList($config, 'accesslist', 'deny', $validateIp) !== false
                && (new HostMatch())->matchList($config, 'accesslist', 'allow', $validateIp) !== true
            ) {
                throw new ForbiddenResponse();
            }
        } elseif ($order == 'allow,deny') {
            // First, all Allow directives are evaluated; at least one must match, or the request is rejected. Next,
            // all Deny directives are evaluated. If any matches, the request is rejected. Last, any requests which do
            // not match an Allow or a Deny directive are denied by default.
            if (
                !(
                    (new HostMatch())->matchList($config, 'accesslist', 'allow', $validateIp) === true
                    && (new HostMatch())->matchList($config, 'accesslist', 'deny', $validateIp) !== true
                )
            ) {
                throw new ForbiddenResponse();
            }
        } else {
            throw new \RuntimeException('Invalid order: ' . $order);
        }
    }
}
