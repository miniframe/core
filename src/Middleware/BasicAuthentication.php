<?php

namespace Miniframe\Middleware;

use Miniframe\Core\AbstractMiddleware;
use Miniframe\Core\Config;
use Miniframe\Core\Request;
use Miniframe\Core\Response;
use Miniframe\Response\RedirectResponse;
use Miniframe\Response\UnauthorizedResponse;
use RuntimeException;

/**
 * Basic HTTP Authentication class
 *
 * This middleware adds basic HTTP authentication to your application.
 */
class BasicAuthentication extends AbstractMiddleware
{
    /**
     * The username
     *
     * @var string|null
     */
    protected $username = null;

    /**
     * Forces the user to be logged in
     *
     * @param Request $request Reference to the Request object.
     * @param Config  $config  Reference to the Config object.
     */
    public function __construct(Request $request, Config $config)
    {
        parent::__construct($request, $config);
        $continueWhenNoCredentials = false;

        // Checks if logging in is required in the current state
        if (!$this->loginRequired()) {
            $continueWhenNoCredentials = true;
        }

        // Check if the current path is part of the exclusion list
        $fullPath = '/' . implode('/', $request->getPath()); // $_SERVER['REQUEST_URI'] can't be used since
                                                                      // that doesn't apply on Console requests
        $excludeList =
            $config->has('authentication', 'exclude')
                ? (array)$config->get('authentication', 'exclude')
                : [];
        foreach ($excludeList as $excludePath) {
            if (!is_string($excludePath)) {
                // @codeCoverageIgnoreStart
                throw new RuntimeException('Invalid exclude path: ' . var_export($excludePath, true));
                // @codeCoverageIgnoreEnd
            }
            $regex = '/^' . str_replace(['?', '\\*'], ['.', '.*?'], preg_quote($excludePath, '/')) . '$/';
            if (preg_match($regex, $fullPath)) {
                $continueWhenNoCredentials = true;
            }
        }

        // Fetches the realm
        $realm = $this->config->get('authentication', 'realm');
        if (!is_string($realm)) {
            // @codeCoverageIgnoreStart
            $realm = 'Basic authentication';
            // @codeCoverageIgnoreEnd
        }

        // Check if we have credentials
        if (
            !is_string($request->getServer('PHP_AUTH_USER')) ||
            !is_string($request->getServer('PHP_AUTH_PW'))
        ) {
            if ($continueWhenNoCredentials) {
                return;
            } else {
                throw new UnauthorizedResponse($realm);
            }
        }

        // Fetch user table
        $userTable = $this->getUserTable();

        // Is the username valid?
        if (!isset($userTable[$request->getServer('PHP_AUTH_USER')])) {
            throw new UnauthorizedResponse($realm);
        }

        // Is the password valid?
        if (!password_verify($request->getServer('PHP_AUTH_PW'), $userTable[$request->getServer('PHP_AUTH_USER')])) {
            throw new UnauthorizedResponse($realm);
        }

        // We passed the BasicAuthentication middleware, continue.
        $this->username = $request->getServer('PHP_AUTH_USER');
    }

    /**
     * Returns a response that forces the user to log out
     *
     * @return Response
     */
    public function logout(): Response
    {
        $host = $this->request->getServer('HTTP_HOST');
        $url = $this->request->isHttpsRequest() ? 'https' : 'http';
        $url .= '://logout@' . (is_string($host) ? $host : '') . $this->config->get('framework', 'base_href');
        return new RedirectResponse($url);
    }

    /**
     * Checks if logging in is required in the current state
     *
     * @return boolean
     */
    protected function loginRequired(): bool
    {
        // Don't do authentication on shell requests
        if ($this->request->isShellRequest()) {
            return false;
        }

        return true;
    }

    /**
     * Returns the user table
     *
     * @return array<string, string>
     */
    protected function getUserTable(): array
    {
        // Fetch user table
        $userTable = $this->config->get('authentication', 'user');
        if (!is_array($userTable)) {
            throw new \RuntimeException('The usertable is invalid. Please check your configuration.');
        }
        return $userTable;
    }

    /**
     * Returns the username
     *
     * @return string|null
     */
    public function getUsername(): ?string
    {
        return $this->username;
    }
}
