# Basic HTTP Authentication

This middleware provides a simple authentication method.
When this middleware is loaded, all page requests are placed after a login.

Some pages could be excluded by configuration. 

## Configuration directives

This middleware requires the following configuration directives:

This directive tells the framework to load the BasicAuthentication middleware:
```ini
[framework]
middleware[] = Miniframe\Middleware\BasicAuthentication
```

In the login popup (which can't be styled since it's a Browser popup), a realm is mentioned to which we want to login.
The test here should be configured with this directive:
```ini
[authentication]
realm = My secret place
```

This user database is also provided in the .ini file.
Passwords can be hashed with the `password_hash("Welcome123", PASSWORD_DEFAULT)` method in PHP.
Multiple users can be specified this way.
```ini
[authentication]
user[stefan] = $2y$10$Gp/MVZmhbdC6sJKRutu.JOZXIjpOycbcuYnjOwJ8TJjFDV4slQIVC
```

If specific pages shouldn't require a login, you can specify exclusions.
The available wildcards are `*` (one or more characters) and `?` (one character).
```ini
[authentication]
exclude[] = /public/*
```

## Hashing a password from the command line

If you're using Docker, you can use this command line:
```cmd
C:\> docker run garrcomm/php-apache-composer:7.3 php -r "echo password_hash('Welcome123', PASSWORD_DEFAULT);"
```

If you're in bash, you can run directly:
```console
$ php -r "echo password_hash('Welcome123', PASSWORD_DEFAULT).PHP_EOL;"
```
