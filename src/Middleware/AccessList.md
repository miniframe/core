# Access Listing

This middleware provides a simple method to control access of clients by their IP address.
When this middleware is loaded, all page requests are validated towards the IP lists.

This middleware is based on the Apache Module [mod_access_compat](https://httpd.apache.org/docs/2.4/mod/mod_access_compat.html) with a few tweaks to make it easier in use.
One of the major differences is that, if you only specify `allow`, all other hosts will be denied and vice versa.
Also, matches are more specific. No wildcard hostname matches, and `192.168` should be notated as `192.168.*.*`.

## Configuration directives

First of all, this is a required directive that tells the framework to use this middleware:
```ini
[framework]
middleware[] = Miniframe\Middleware\AccessList
```

This middleware has three directives; `allow`, `deny` and `order`.<br>
The `allow` and `deny` directives contain IP lists.
The `order` directive is a bit more complex and explained in the [Using the order directive](#using-the-order-directive) section. 

The "Allow" list could be like this;
```ini
[accesslist]
; Allow local IPv4 addresses with wildcard
allow[] = 192.168.2.*
; Allow a specific IPv4 address
allow[] = 8.8.8.8
```

CIDR masks are also enabled. This configuration allows all private networks with a CIDR mask (See https://en.wikipedia.org/wiki/Private_network for the ranges)
```ini
[accesslist]
allow[] = 10.0.0.0/8
allow[] = 172.16.0.0/12
allow[] = 192.168.0.0/16
```

For IPv6 support, wildcards are not enabled but CIDR masks are still possible;
```ini
[accesslist]
; Allow IPv6 addresses
allow[] = 21DA:00D3:0000:2F3B:02AC:00FF:FE28:9C5A
; Allow IPv6 range with CIDR mask
allow[] = 21DA:00D3:0000:2F3B::/64
```

Hostnames are resolved before they match. Keep in mind this slows down the application. So this works;
```ini
[accesslist]
allow[] = dns.google ; resolves to 2001:4860:4860::8844, 2001:4860:4860::8888, 8.8.4.4 & 8.8.8.8
```

Instead of `allow[]`, you can also use `deny[]` turning the Allow listing into Deny listing.
All options work the same, wildcards, CIDR masks, etcetera;

```ini
[accesslist]
deny[] = 8.8.8.8
```

## Using the order directive

Using both `allow[]` and `deny[]` simultaneously will work perfectly, but the order matters.

By default, the order is `Deny,Allow`<br>
First, all Deny directives are evaluated; if any match, the request is denied unless it also matches an Allow directive. Any requests which do not match any Allow or Deny directives are permitted.

This can be changed to `Allow,Deny`<br>
First, all Allow directives are evaluated; at least one must match, or the request is rejected. Next, all Deny directives are evaluated. If any matches, the request is rejected. Last, any requests which do not match an Allow or a Deny directive are denied by default.

Basically, if you follow this table, you're good:

| Match                     | `Allow,Deny` result                 | `Deny,Allow` result                  |
| ------------------------- | ----------------------------------- | ------------------------------------ |
| Matches Allow only        | Request allowed                     | Request allowed                      |
| Matches Deny only         | Request denied                      | Request denied                       |
| No match                  | Default to second directive: Denied | Default to second directive: Allowed |
| Matches both Allow & Deny | Final match controls: Denied        | Final match controls: Allowed        |

### Example 1

```ini
[accesslist]
order   = allow,deny
allow[] = 192.168.2.100
allow[] = 172.16.0.1
deny[]  = 192.168.2.*
```

* **192.168.2.100** - Matches both Allow & Deny: Final match controls: Denied
* **192.168.2.1** - Matches Deny only: Request denied
* **10.0.0.1** - No match: Default to second directive: Denied
* **172.16.0.1** - Matches Allow only: Request allowed

### Example 2

```ini
[accesslist]
order   = deny,allow
allow[] = 192.168.2.100
allow[] = 172.16.0.1
deny[]  = 192.168.2.*
```

* **192.168.2.100** - Matches both Allow & Deny: Final match controls: Allowed
* **192.168.2.1** - Matches Deny only: Request denied
* **10.0.0.1** - No match: Default to second directive: Allowed
* **172.16.0.1** - Matches Allow only: Request allowed
