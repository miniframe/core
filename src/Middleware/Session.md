# Session handling

This middleware provides a simple method to read and write from PHP sessions.
As soon as you read or write to the session handler, a session will be started.

## Configuration directives

You'll need the required directive that tells the framework to use this middleware:
```ini
[framework]
middleware[] = Miniframe\Middleware\Session
```

## How to use

The session handler works with a simple get/set system:

```php
$session = Miniframe\Core\Registry::get(\Miniframe\Middleware\Session::class);
var_dump($session->get('key'));
$session->set('key', 'value');
```

But when a session is opened, all connections from that client wait until the session is closed.
Especially with file downloads, this can be annoying. To prevent that, call:

```php
$session = Miniframe\Core\Registry::get(\Miniframe\Middleware\Session::class);
$session->commit();
```