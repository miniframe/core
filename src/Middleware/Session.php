<?php

namespace Miniframe\Middleware;

use Miniframe\Core\AbstractMiddleware;

class Session extends AbstractMiddleware
{
    /**
     * True when a session is active
     *
     * @var bool
     */
    private $sessionStarted = false;

    /**
     * Gets a session value
     *
     * @param string $key Name of the session item.
     *
     * @return mixed
     */
    public function get(string $key)
    {
        $this->begin();
        return $_SESSION[$key] ?? null;
    }

    /**
     * Sets a session value
     *
     * @param string $key   Name of the session item.
     * @param mixed  $value Value to put in the session.
     *
     * @return void
     */
    public function set(string $key, $value): void
    {
        $this->begin();
        $_SESSION[$key] = $value;
    }

    /**
     * Starts the PHP session handler
     *
     * @return void
     */
    public function begin(): void
    {
        if ($this->sessionStarted === true) {
            return;
        }
        session_start();
        $this->sessionStarted = true;
    }

    /**
     * Commit changes to the session handler and releases the connection to prevent processes to wait for each other
     *
     * @return void
     */
    public function commit(): void
    {
        if ($this->sessionStarted === false) {
            return;
        }
        session_write_close();
        $this->sessionStarted = false;
    }

    /**
     * Returns the current session ID, or null when there's no active session
     *
     * @return string|null
     */
    public function getSessionId(): ?string
    {
        if ($this->sessionStarted) {
            return session_id() ?: null;
        }
        return null;
    }
}
