# ForwardedFor Middleware

Sometimes a webserver is behind a proxy. In those cases, some headers are not set correctly.

This middleware corrects that behaviour so we can detect, for example, if we're forwarded from HTTPS to HTTP,
or so we have the correct client IP for access listing.

## Configuration directives

This middleware requires the following configuration directives:

This directive tells the framework to load the ForwardedFor middleware:
```ini
[framework]
middleware[] = Miniframe\Middleware\ForwardedFor
```
Put this above any middleware that needs IP addresses.

For security reasons, it's wise to also include this config entry,
so we only accept ForwardedFor headers from authorized proxy servers:

```ini
[forwarded-for]
proxyhost[] = 10.*.*.*
proxyhost[] = 192.168.*.*
proxyhost[] = 172.16.0.0/12
```

When no `proxyhost` is defined, all hits will be considered a proxy.
Values for `proxyhost` can be IP addresses and hostnames. IP addresses may contain wildcards or SIDR notations.
The example above includes all private network IP addresses.
