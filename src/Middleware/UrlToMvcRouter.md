# URL To MVC Router

This middleware converts URLs like `/index/main` to `App\Controller\Index::main()`.
The first two portions of the URL are converted to the controller name and method name,
where Index is the default controller and main the default method.

So;<br>
`/` will redirect to `App\Controller\Index::main()`.  
`/foo` will redirect to `App\Controller\Foo::main()`.  
`/foo/bar` will redirect to `App\Controller\Foo::bar()`.  
`/foo/bar/baz` will also redirect to `App\Controller\Foo::bar()`.  
`/foo_bar/bar_baz` will redirect to `App\Controller\FooBar::barBaz()`.  
`/fooBar/barBaz` will also redirect to `App\Controller\FooBar::barBaz()`.  
By reading `$this->request->getPath(2)` you'll get the 'barBaz' value.  

The controller must extend `Miniframe\Core\AbstractController`
and the method must return a `Miniframe\Core\Response` object.

## Configuration directives

This middleware requires the following configuration directives:

This directive tells the framework to load the UrlToMvcRouter middleware as router:
```ini
[framework]
middleware[] = Miniframe\Middleware\UrlToMvcRouter
```
This directive tells that routing starts from the root.
This could also be /public if you want all URLs to be prefixed with /public.
```ini
[framework]
base_href = /
```
