<?php

namespace Miniframe\Middleware;

use Miniframe\Core\AbstractController;
use Miniframe\Core\AbstractMiddleware;

/**
 * Load this middleware to use basic routing (GET "/index/main" returns callable App\Controller\Index::main)
 *
 * This is the most basic way of routing
 */
class UrlToMvcRouter extends AbstractMiddleware
{
    /**
     * Returns a reference to the UrlToMvcRouter::requestToMvc method
     *
     * @return callable[]
     */
    public function getRouters(): array
    {
        return [
            array($this, 'requestToMvc'),
        ];
    }

    /**
     * Converts a request path to a controller+method (GET "/index/main" returns callable App\Controller\Index::main)
     *
     * @return callable
     */
    public function requestToMvc(): ?callable
    {
        $path = $this->request->getPath();

        // Remove base path, if present
        $baseHref = $this->config->get('framework', 'base_href');
        if (!is_string($baseHref)) {
            // @codeCoverageIgnoreStart
            $baseHref = '/';
            // @codeCoverageIgnoreEnd
        }
        $basePath = explode('/', trim((string)parse_url($baseHref, PHP_URL_PATH), '/'));
        foreach ($basePath as $prefix) {
            if (isset($path[0]) && $path[0] == $prefix) {
                array_shift($path);
                $path = array_values($path); // Reset indexes
            }
        }

        if (count($path) == 0) {
            $controller = 'Index';
            $method = 'main';
        } elseif (count($path) == 1) {
            $controller = $this->snakeCaseToUpperCamelCase($path[0]);
            $method = 'main';
        } else {
            $controller = $this->snakeCaseToUpperCamelCase($path[0]);
            $method = $this->snakeCaseToLowerCamelCase($path[1]);
        }

        // Add Controller namespace
        $controller = '\\App\\Controller\\' . $controller;
        if (!class_exists($controller)) {
            return null;
        }
        if (!is_subclass_of($controller, AbstractController::class)) {
            throw new \RuntimeException('Controller invalid: ' . $controller . ' not a ' . AbstractController::class);
        }
        $callable = [new $controller($this->request, $this->config), $method];
        if (!is_callable($callable)) {
            return null;
        }

        return $callable;
    }

    /**
     * Converts snake_case_text to UpperCamelCaseText
     *
     * @param string $snakeCase Text as snake_case.
     *
     * @return string
     */
    protected function snakeCaseToUpperCamelCase(string $snakeCase): string
    {
        return str_replace(' ', '', ucwords(str_replace(['_', '-'], [' ', ' '], $snakeCase)));
    }

    /**
     * Converts snake_case_text to lowerCamelCaseText
     *
     * @param string $snakeCase Text as snake_case.
     *
     * @return string
     */
    protected function snakeCaseToLowerCamelCase(string $snakeCase): string
    {
        return lcfirst($this->snakeCaseToUpperCamelCase($snakeCase));
    }
}
