<?php
namespace PHPSTORM_META {

    // When using Registry::get(Session::class), it returns a Session object. This makes PHPStorm aware of that
    override(\Miniframe\Core\Registry::get(), map([
        \Factory::classConstant => '@Class',
    ]));

    // Suggests constants as fourth argument in StreamResponse::__construct()
    expectedArguments(
        \Miniframe\Response\StreamResponse::__construct(),
        3,
        \Miniframe\Response\StreamResponse::DISPOSITION_ATTACHMENT,
        \Miniframe\Response\StreamResponse::DISPOSITION_INLINE
    );
}
