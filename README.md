# Miniframe PHP Framework Core Bundle

This is the core package of the Miniframe PHP Framework. For more information, see the [Main project](https://bitbucket.org/miniframe/miniframe-php-framework) at [https://bitbucket.org/miniframe/miniframe-php-framework](https://bitbucket.org/miniframe/miniframe-php-framework).

[![build](https://miniframe.dev/build/badge/core)](https://bitbucket.org/miniframe/core/addon/pipelines/home)
[![code coverage](https://miniframe.dev/codecoverage/badge/core)](https://miniframe.dev/codecoverage/core)

## Core classes explained

This framework contains a few core classes. These classes are used in this core:

| Class                                                 | Description                                                                         |
| ----------------------------------------------------- | ----------------------------------------------------------------------------------- |
| [AbstractController](src/Core/AbstractController.php) | All Controllers should extend this. It contains the `Request` and `Config` object.  |
| [AbstractMiddleware](src/Core/AbstractMiddleware.php) | All Middlewares should extend this. It contains the `Request` and `Config` object.  |
| [Bootstrap](src/Core/Bootstrap.php)                   | The kickstarter of this framework.                                                  |
| [Config](src/Core/Config.php)                         | Parses the configuration files and provides an interface to request it's contents.  |
| [Registry](src/Core/Registry.php)                     | Registry that can be used to store and later retrieve objects.                      |
| [Request](src/Core/Request.php)                       | Parses the web request and provides an interface to read the request.               |
| [Response](src/Core/Response.php)                     | Populate (or extend) this object in a Controller to return something to the client. |

## Middlewares included in the Core bundle

In the [main project](https://bitbucket.org/miniframe/miniframe-php-framework), you'll see a `middleware` directive in the configuration.
Multiple middlewares can be loaded, and are loaded in the sequence in which they are configured.

These middlewares are included in this Miniframe Core bundle and can be used to quickly set up a basic application, or as example for building your own middleware classes:

| Class name                                                    | Description                                                                                                                   |
|---------------------------------------------------------------|-------------------------------------------------------------------------------------------------------------------------------|
| [UrlToMvcRouter](src/Middleware/UrlToMvcRouter.php)           | URL To MVC Router; documentation at [src/Middleware/UrlToMvcRouter.md](src/Middleware/UrlToMvcRouter.md)                      |
| [BasicAuthentication](src/Middleware/BasicAuthentication.php) | Basic HTTP Authentication; documentation at [src/Middleware/BasicAuthentication.md](src/Middleware/BasicAuthentication.md)    |
| [Session](src/Middleware/Session.php)                         | PHP Sessions; documentation at [src/Middleware/Session.md](src/Middleware/Session.md)                                         |
| [AccessList](src/Middleware/AccessList.php)                   | Deny / Allow clients based by their IP; documentation at [src/Middleware/AccessList.md](src/Middleware/AccessList.md)         |
| [ForwardedFor](src/Middleware/ForwardedFor.php)               | Parsing X-Forwarded-* headers when proxied; documentation at [src/Middleware/ForwardedFor.md](src/Middleware/ForwardedFor.md) |

A full list of middlewares can be found at [https://miniframe.dev/middlewares](https://miniframe.dev/middlewares)

## Pre-defined response types included in the Core bundle

| Class name                                                                  | Type    | Description                                     |
| --------------------------------------------------------------------------- | ------- | ----------------------------------------------- |
| [Response](src/Core/Response.php)                                           | Success | Main Response object, extended by all others.   |
| [JsonResponse](src/Response/JsonResponse.php)                               | Success | Returns the data formatted as JSON.             |
| [PhpResponse](src/Response/PhpResponse.php)                                 | Success | Uses PHP templates to create a proper response. |
| [StreamResponse](src/Response/StreamResponse.php)                           | Success | A basic 200 OK response for large files.        |
| [RedirectResponse](src/Response/RedirectResponse.php)                       | Success | A basic 30? Redirect page.                      |
| [UnauthorizedResponse](src/Response/UnauthorizedResponse.php)               | Error   | A basic 401 Unauthorized page.                  |
| [ForbiddenResponse](src/Response/ForbiddenResponse.php)                     | Error   | A basic 403 Forbidden page.                     |
| [NotFoundResponse](src/Response/NotFoundResponse.php)                       | Error   | A basic 404 Not Found page.                     |
| [InternalServerErrorResponse](src/Response/InternalServerErrorResponse.php) | Error   | A basic 500 Internal Server Error page.         |

A Response object can be returned in a Controller method or being thrown.
Throwing responses can especially be useful for error pages.

## Tip for Windows Developers

In the `bin` folder, a few batch files exist, to make development easier.

If you install [Docker Desktop for Windows](https://www.docker.com/products/docker-desktop),
you can use [bin\composer.bat](bin/composer.bat), [bin\phpcs.bat](bin/phpcs.bat), [bin\phpunit.bat](bin/phpunit.bat), [bin\phpstan.bat](bin/phpstan.bat) and [bin\security-checker.bat](bin\security-checker.bat) as shortcuts for Composer, CodeSniffer, PHPUnit, PHPStan and the Security Checker, without the need of installing PHP and other dependencies on your machine.

The same Docker container and tools are used in [Bitbucket Pipelines](https://bitbucket.org/miniframe/core/addon/pipelines/home) to automatically test this project.
